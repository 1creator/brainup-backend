<?php

namespace App\Providers;

use App\Services\Admin\AdminService;
use App\Services\App\AppService;
use App\Services\App\AppSessionService;
use App\Services\App\DynamicAppService;
use App\Services\App\StaticAppService;
use App\Services\Attachment\Services\AttachmentService;
use App\Services\Group\GroupService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
