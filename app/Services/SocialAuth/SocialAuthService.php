<?php


namespace App\Services\SocialAuth;


use Doctrine\DBAL\Driver\PDO\MySQL\Driver;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthService
{
    public function getRedirect($modelClass, $data)
    {
//        $redirect = config("services.${data['provider']}.redirect");
//        config([
//            "services.${data['provider']}.redirect" => $redirect . '?modelClass=' . $modelClass,
//        ]);
        return Socialite::driver($data['provider'])->stateless()->redirect();
    }

    public function login($modelClass, $provider)
    {
        try {
            $userProvided = Socialite::driver($provider)->stateless()->user();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect('/');
        }

        $user = $modelClass::where('email', $userProvided->getEmail())->first();
        $token = Hash::make($userProvided->token);
        if (!$user) {
            $splitName = explode(' ', $userProvided->getName(), 2);
            $user = new $modelClass([
                'first_name' => $splitName[0],
                'last_name' => $splitName[1],
                'email' => $userProvided->getEmail(),
            ]);
            $user->password = $token;
            $user->save();
        }

        return redirect(config('services.frontend_url') . '/student' . '?token=' . $user->api_token);
    }
}
