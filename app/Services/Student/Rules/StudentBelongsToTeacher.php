<?php


namespace App\Services\Student\Rules;


use App\Models\Student;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class StudentBelongsToTeacher implements Rule
{

    public function passes($attribute, $value)
    {
        /** @var Student $student */
        $student = Student::with('group')->findOrFail($value);
        return $student->group->teacher_id ?? null == Auth::guard('teacher')->id();
    }

    public function message()
    {
        return 'Ученик не является участником ни одной из ваших групп.';
    }
}
