<?php


namespace App\Services\Student\QueryBuilder;


use App\Models\BrainValue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Spatie\QueryBuilder\Sorts\Sort;

class StudentBrainsSort implements Sort
{

    public function __invoke(Builder $query, bool $descending, string $property)
    {
//        return $query->join('brain_values', 'brain_values.student_id', '=', 'students.id')
//            ->select(['students.*', 'brain_values.value as brains', 'brain_values.date'])
//            ->groupBy('students.id')
//            ->orderByRaw('max(database.brain_values.value) desc');

        return $query->leftJoin('brain_values', function (JoinClause $join) {
            $join->on('brain_values.student_id', '=', 'students.id')
                ->on('brain_values.date', '=', DB::raw('(SELECT MAX(brain_values.date) FROM brain_values WHERE student_id = students.id)'));
        })->select(['students.*', 'brain_values.value as brains'])
            ->orderByDesc('brains');


//            ->orderByRaw('max(brain_values.value) desc');

//        return $query->orderByDesc(BrainValue::select('value')
//            ->whereColumn('brain_values.student_id', 'students.id')
//            ->orderByDesc('value')
//            ->take(1)
//        );
    }
}
