<?php


namespace App\Services\Student;


use App\Models\Student;
use App\Models\StudentInvite;
use App\Models\Subscription;
use App\Models\SubscriptionTransaction;
use App\Models\Teacher;
use App\Services\Attachment\Models\Attachment;
use App\Services\Student\Notifications\StudentInvited;
use App\Services\Subscription\Notifications\StudentTestSubscriptionNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class StudentService
{
    public function store(array $data): Student
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'first_name' => 'required|string|max:20',
            'last_name' => 'required|string|max:20',
            'middle_name' => 'nullable|string|max:20',
            'birthday' => 'nullable|date',
            'phone' => 'nullable|string|max:20|unique:students',
            'login' => 'required_without:email|string|alpha_dash|max:20|unique:students',
            'email' => 'required_without:login|nullable|email:strict|unique:students',
            'password' => 'required|confirmed|min:4',
        ]);

        $student = new Student($data->only([
            'group_id', 'last_name', 'first_name', 'middle_name', 'login', 'birthday', 'phone', 'email', 'city',
        ])->toArray());
        $student->password = Hash::make($data['password']);

        $user = Auth::guard('teacher')->user();
        if ($user instanceof Teacher) {
            if ($data->has('group_id')) {
                $student->group_id = $data['group_id'];
            } else {
                $student->group_id = $user->defaultGroup->id;
            }
        }

        $student->save();
        $student->makeVisible('api_token');

        $data = DB::table('free_apps')->select('app_id')->get()->map(
            function ($item) use ($student) {
                return [
                    'student_id' => $student->id,
                    'app_id' => $item->app_id,
                ];
            })->toArray();
        DB::table('student_app')->insert($data);

        return $student;
    }

    public function invite(Teacher $teacher, Student $student): bool
    {
        StudentInvite::create([
            'group_id' => $teacher->defaultGroup->id,
            'student_id' => $student->id,
        ]);

        $notification = new StudentInvited($teacher, $student);
        $student->notify($notification);
        return 1;
    }

    public function update(Student $student, array $data): Student
    {
        $data = collect($data);

        $rules = [
            'first_name' => 'sometimes|required|string|max:30',
            'last_name' => 'sometimes|required|string|max:30',
            'middle_name' => 'sometimes|nullable|string|max:20',
            'email' => 'sometimes|nullable|email|unique:students,email,' . $student->id,
            'login' => 'sometimes|string|alpha_dash|max:20|unique:students,login,' . $student->id,
            'phone' => 'sometimes|nullable|string|max:20|unique:students,phone,' . $student->id,
            'birthday' => 'sometimes|nullable|date',
        ];

        if ($data->has('password') && $data['password']) {
            $rules['password'] = 'sometimes|confirmed|string|min:4';
            $rules['current_password'] = [
                'sometimes',
                'nullable',
                'required_with:password',
                function ($attribute, $value, $fail) use ($student) {
                    if (!Hash::check($value, $student->password)) {
                        $fail($attribute . ' is invalid.');
                    }
                }];
        }

        Validator::validate($data->toArray(), $rules);

        $student->fill($data->only([
            'first_name', 'last_name', 'middle_name', 'birthday', 'email', 'phone', 'school', 'group_id', 'login'
        ])->toArray());

        if ($data->has('password') && $data['password']) {
            $student->password = Hash::make($data['password']);
        }

        if ($data->has('image_id') && $data['image_id'] != ($student->image->id ?? null)) {
            $student->image()->delete();
            if ($data['image_id']) {
                $attachment = Attachment::findOrFail($data['image_id']);
                $student->image()->save($attachment);
            }
            $student->load('image');
        }

        $student->save();
        return $student;
    }

    public function giveTestSubscription(Student $student, $password = null): Student
    {
        $subscription = new Subscription();
        $subscription->user()->associate($student);
        $subscription->expired_at = Carbon::now()->addMonth();
        $subscription->save();

        $subscriptionTransaction = new SubscriptionTransaction([
            'type' => 'accept',
            'description' => 'Получена пробная подписка (1 месяц доступа).'
        ]);

        $subscriptionTransaction->owner()->associate($student);
        $subscriptionTransaction->save();

        Log::info($student->email);
        if ($student->email) {
            $student->notify(new StudentTestSubscriptionNotification($password));
        }

        return $student;
    }
}
