<?php


namespace App\Services\Student\Notifications;


use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;

class StudentInvited extends Notification
{

    private Teacher $teacher;
    private Student $student;

    public function __construct(Teacher $teacher, Student $student)
    {
        $this->teacher = $teacher;
        $this->student = $student;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $teacherName = $this->teacher->full_name;
        $url = config('services.frontend_url') . '/student/group?action=check-invites';

        return (new MailMessage)
            ->subject('Приглашение от преподавателя')
            ->greeting('Привет!')
            ->line("Преподаватель **${teacherName}** на сайте brain-up.ru пригласил тебя вступить в его группу!")
            ->action('Вступить', $url);
    }
}
