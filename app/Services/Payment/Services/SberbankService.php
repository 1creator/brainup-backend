<?php


namespace App\Services\Payment\Services;


use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;

class SberbankService implements PaymentInterface
{
    private Client $client;
    protected ?InteractsWithPaymentsInterface $payer;

    public function __construct(InteractsWithPaymentsInterface $payer = null)
    {
        $this->client = new Client([
            'userName' => config('services.sberbank.username'),
            'password' => config('services.sberbank.password'),

            'language' => 'ru',

            // A currency code in ISO 4217 format.
            // Use this option to set a currency used by default.
            'currency' => Currency::RUB,

            // An uri to send requests.
            // Use this option if you want to use the Sberbank's test server.
//            'apiUri' => Client::API_URI_TEST,

//            // An HTTP method to use in requests.
//            // Must be "GET" or "POST" ("POST" is used by default).
//            'httpMethod' => HttpClientInterface::METHOD_GET,
        ]);
        $this->payer = $payer;
    }

    /** @noinspection PhpInconsistentReturnPointsInspection */
    public function makeIframe(int $amount, string $description, $product = null, $info = null): string
    {
        abort(404, 'Этот способ оплаты не реализован.');
    }

    /**
     * @param int $amount
     * @param string $description
     * @param string $return_url
     * @param array|null $product
     * @param array|null $info
     * @return string
     */
    public function makeRedirect(int $amount, string $description, string $return_url, $product = null, $info = null): string
    {
        if ($amount == 0) throw new InvalidArgumentException('Field amount in transaction can not be null.');

        $orderId = Str::random(32);

        $payment = new Payment([
            'id' => $orderId,
            'owner_type' => get_class($this->payer),
            'owner_id' => $this->payer->getPayerId(),
            'amount' => $amount,
            'currency' => 'RUB',
            'description' => $description,
            'status' => 'pending',
        ]);

        if ($product) {
            $payment['product_type'] = $product['product_type'] ?? null;
            $payment['product_id'] = $product['product_id'] ?? null;
        }

        if ($info) {
            $payment['info'] = $info;
        }

        $payment->save();

        $return_url .= '?payment=' . $payment['id'];

        $response = $this->client->registerOrder($orderId, $amount, $return_url);
        return $response['formUrl'];
    }

    public function resolveWebHook(Request $request)
    {
        abort(404, 'Этот способ оплаты не реализован.');
        /*   Log::info($request);
           switch ($request->input(['event'])) {
               case NotificationEventType::PAYMENT_SUCCEEDED:
                   $notification = new NotificationSucceeded($request->all());
                   $yandexPayment = $notification->getObject();
                   $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                   $payment->status = 'succeeded';
                   if ($payment->isDirty('status')) {
                       event(new PaymentSucceeded($payment));
                       Log::info("payment success webhook and status is dirty, sending notification...");
                   } else {
                       Log::info("payment success webhook, but status is not dirty");
                   }
                   $payment->save();
                   break;
               case NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE:
                   $yandexPayment = new NotificationWaitingForCapture($request->all());
                   $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                   $payment->status = 'waiting_for_capture';
                   if ($payment->isDirty('status')) {
                       event(new PaymentWaitingForCapture($payment));
                   }
                   $payment->save();
                   break;
               case NotificationEventType::PAYMENT_CANCELED:
                   $yandexPayment = new NotificationCanceled($request->all());
                   $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                   $payment->status = 'cancelled';
                   if ($payment->isDirty('status')) {
                       event(new PaymentCanceled($payment));
                   }
                   $payment->save();
                   break;
               case NotificationEventType::REFUND_SUCCEEDED:
                   $yandexPayment = new NotificationRefundSucceeded($request->all());
                   $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                   $payment->status = 'refunded';
                   if ($payment->isDirty('status')) {
                       event(new PaymentRefund($payment));
                   }
                   $payment->save();
                   break;
           }

           return 1;*/
    }
}
