<?php


namespace App\Services\Payment\Services;


use App\Services\Payment\AnonymousPayer;
use App\Services\Payment\Events\PaymentCanceled;
use App\Services\Payment\Events\PaymentRefund;
use App\Services\Payment\Events\PaymentSucceeded;
use App\Services\Payment\Events\PaymentWaitingForCapture;
use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use YandexCheckout\Client;
use YandexCheckout\Model\Notification\NotificationCanceled;
use YandexCheckout\Model\Notification\NotificationRefundSucceeded;
use YandexCheckout\Model\Notification\NotificationSucceeded;
use YandexCheckout\Model\Notification\NotificationWaitingForCapture;
use YandexCheckout\Model\NotificationEventType;
use YandexCheckout\Request\Payments\CreatePaymentResponse;

class YandexKassaService implements PaymentInterface
{
    private Client $client;
    protected ?InteractsWithPaymentsInterface $payer;

    public function __construct(InteractsWithPaymentsInterface $payer = null)
    {
        $this->client = new Client();
        $this->client->setAuth(env('YANDEX_SHOP_ID'), env('YANDEX_SHOP_KEY'));
        $this->payer = $payer;
    }

    public function makeIframe(int $amount, string $description): Payment
    {
        if ($amount == 0) throw new InvalidArgumentException('Field amount in transaction can not be null.');

        $response = $this->client->createPayment(
            [
                'amount' => [
                    'value' => $amount,
                    'currency' => 'RUB',
                ],
                'confirmation' => [
                    'type' => 'embedded'
                ],
                'capture' => true,
                'description' => $description,
                "receipt" => [
                    "customer" => [
                        "full_name" => $this->payer->getPayerFullName(),
                        "phone" => $this->payer->getPayerPhone(),
                        "email" => $this->payer->getPayerEmail(),
                    ],
                    "items" => [
                        [
                            "description" => $description,
                            "quantity" => "1.00",
                            "amount" => [
                                "value" => $amount,
                                "currency" => "RUB"
                            ],
                            "vat_code" => "2",
                            "payment_mode" => "full_prepayment",
                            "payment_subject" => "commodity"
                        ],
                    ]
                ],
            ],
            uniqid('', true)
        );
        return $this->createPayment($response);
    }

    public function makeRedirect(int $amount, string $description, string $return_url): Payment
    {
        if ($amount == 0) throw new InvalidArgumentException('Field amount in transaction can not be null.');

        $response = $this->client->createPayment(
            array(
                'amount' => [
                    'value' => $amount,
                    'currency' => 'RUB',
                ],
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => $return_url,
                ),
                'capture' => true,
                'description' => $description,
                "receipt" => [
                    "customer" => [
                        "full_name" => $this->payer->getPayerFullName(),
                        "phone" => $this->payer->getPayerPhone(),
                        "email" => $this->payer->getPayerEmail(),
                    ],
                    "items" => [
                        [
                            "description" => $description,
                            "quantity" => "1.00",
                            "amount" => [
                                "value" => $amount,
                                "currency" => "RUB"
                            ],
                            "vat_code" => "2",
                            "payment_mode" => "full_prepayment",
                            "payment_subject" => "commodity"
                        ],
                    ]
                ],
            ),
            uniqid('', true)
        );
        return $this->createPayment($response);
    }

    public function resolveWebHook(Request $request)
    {
        Log::info($request);
        switch ($request->input(['event'])) {
            case NotificationEventType::PAYMENT_SUCCEEDED:
                $notification = new NotificationSucceeded($request->all());
                $yandexPayment = $notification->getObject();
                $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                $payment->status = 'succeeded';
                if ($payment->isDirty('status')) {
                    event(new PaymentSucceeded($payment));
                    Log::info("payment success webhook and status is dirty, sending notification...");
                } else {
                    Log::info("payment success webhook, but status is not dirty");
                }
                $payment->save();
                break;
            case NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE:
                $yandexPayment = new NotificationWaitingForCapture($request->all());
                $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                $payment->status = 'waiting_for_capture';
                if ($payment->isDirty('status')) {
                    event(new PaymentWaitingForCapture($payment));
                }
                $payment->save();
                break;
            case NotificationEventType::PAYMENT_CANCELED:
                $yandexPayment = new NotificationCanceled($request->all());
                $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                $payment->status = 'cancelled';
                if ($payment->isDirty('status')) {
                    event(new PaymentCanceled($payment));
                }
                $payment->save();
                break;
            case NotificationEventType::REFUND_SUCCEEDED:
                $yandexPayment = new NotificationRefundSucceeded($request->all());
                $payment = Payment::findOrFail('yandex_' . $yandexPayment->id);
                $payment->status = 'refunded';
                if ($payment->isDirty('status')) {
                    event(new PaymentRefund($payment));
                }
                $payment->save();
                break;
        }

        return 1;
    }

    private function createPayment(CreatePaymentResponse $yandexPayment): Payment
    {
        $payment = new Payment([
            'id' => 'yandex_' . $yandexPayment->id,
            'owner_type' => get_class($this->payer),
            'owner_id' => $this->payer->getPayerId(),
            'paid' => $yandexPayment->paid,
            'amount' => $yandexPayment->amount->value,
            'currency' => $yandexPayment->amount->currency,
            'confirmation' => $yandexPayment->confirmation,
            'description' => $yandexPayment->description,
            'refundable' => $yandexPayment->refundable,
        ]);
        $payment->save();
        return $payment->fresh();
    }
}
