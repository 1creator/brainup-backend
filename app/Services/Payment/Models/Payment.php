<?php


namespace App\Services\Payment\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail(string $string)
 */
class Payment extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    protected $casts = [
        'info' => 'array',
    ];

    public function owner()
    {
        return $this->morphTo('owner');
    }
}
