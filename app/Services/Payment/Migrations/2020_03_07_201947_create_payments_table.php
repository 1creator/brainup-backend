<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->morphs('owner');
            $table->morphs('product');
            $table->float('amount');
            $table->string('currency');
            $table->string('description')->nullable();
            $table->json('info')->nullable();
            $table->string('status')->nullable();
            $table->boolean('refundable')->default(false);
            $table->dateTime('captured_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
