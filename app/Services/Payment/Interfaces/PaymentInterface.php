<?php


namespace App\Services\Payment\Interfaces;


use Illuminate\Http\Request;

interface PaymentInterface
{
    public function makeIframe(float $amount, string $description, $product = null, $info = null): string;

    public function makeRedirect(float $amount, string $description, string $return_url, $product = null, $info = null): string;

    public function resolveWebHook(Request $request);
}
