<?php


namespace App\Services\Payment\Interfaces;


use Illuminate\Database\Eloquent\Relations\MorphMany;

interface InteractsWithPaymentsInterface
{
    public function payments(): MorphMany;

    public function getPayerId(): int;

    public function getPayerFullName(): ?string;

    public function getPayerPhone(): ?string;

    public function getPayerEmail(): ?string;
}
