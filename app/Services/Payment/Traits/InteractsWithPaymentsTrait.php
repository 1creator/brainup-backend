<?php /** @noinspection ALL */

namespace App\Services\Payment\Traits;


use App\Services\Payment\Models\Payment;
use App\Services\Payment\Models\Transaction;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait InteractsWithPaymentsTrait
{
    public function payments(): MorphMany
    {
        return $this->morphMany(Payment::class, 'owner')->orderBy('created_at', 'desc');
    }

    public function getPayerId(): int
    {
        return $this->id;
    }

    public function getPayerFullName(): ?string
    {
        return implode(' ', [$this->last_name, $this->first_name, $this->middle_name]);
    }

    public function getPayerPhone(): ?string
    {
        if (!$this->phone) return null;

        $phone = preg_replace('/[^0-9]/', '', $this->phone);
        if (strlen($phone) != 11) return null;
        if ($phone[0] == '8') $phone[0] = '7';
        return $this->phone;
    }

    public function getPayerEmail(): ?string
    {
        return $this->email;
    }
}
