<?php


namespace App\Services\Payment\Events;


use App\Services\Payment\Models\Payment;

class PaymentCanceled
{
    public Payment $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }
}
