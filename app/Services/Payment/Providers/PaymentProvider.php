<?php

namespace App\Services\Payment\Providers;

use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Services\ProdamusService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class PaymentProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentInterface::class, ProdamusService::class);

        $this->app->when(ProdamusService::class)
            ->needs(InteractsWithPaymentsInterface::class)
            ->give(function () {
                try {
                    return Auth::user();
                } catch (\Exception $e) {
                    return null;
                }
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
