<?php


namespace App\Services\Teacher;


use App\Models\Subscription;
use App\Models\SubscriptionTransaction;
use App\Models\Teacher;
use App\Services\Attachment\Models\Attachment;
use App\Services\Subscription\Notifications\TeacherTestSubscriptionNotification;
use App\Services\Teacher\Events\TeacherCreated;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TeacherService
{
    public function store(array $data): Teacher
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'first_name' => 'required|string|max:20',
            'last_name' => 'required|string|max:20',
            'middle_name' => 'nullable|string|max:20',
            'birthday' => 'nullable|date',
            'city' => 'required|string|max:30',
            'phone' => 'nullable|string|max:20|unique:teachers',
            'email' => 'required|email:strict|unique:teachers',
            'password' => 'required|confirmed|min:4',
        ]);

        $teacher = new Teacher($data->only([
            'last_name', 'first_name', 'middle_name', 'birthday', 'phone', 'email', 'city',
        ])->toArray());
        $teacher->password = Hash::make($data['password']);

        $teacher->save();

        event(new TeacherCreated($teacher));

        return $teacher;
    }

    public function update(Teacher $teacher, array $data): Teacher
    {
        $data = collect($data);

        $rules = [
            'first_name' => 'sometimes|required|string|max:30',
            'last_name' => 'sometimes|required|string|max:30',
            'email' => 'sometimes|email|required|unique:teachers,email,' . $teacher->id,
            'phone' => 'sometimes|nullable|string|max:20|unique:teachers,phone,' . $teacher->id,
            'birthday' => 'sometimes|nullable|date',
            'city' => 'required|string|max:30',
        ];

        if ($data->has('password') && $data['password']) {
            $rules['password'] = 'sometimes|confirmed|string|min:4';
            $rules['current_password'] = [
                'sometimes',
                'nullable',
                'required_with:password',
                function ($attribute, $value, $fail) use ($teacher) {
                    if (!Hash::check($value, $teacher->password)) {
                        $fail($attribute . ' is invalid.');
                    }
                }];
        }

        Validator::validate($data->toArray(), $rules);

        $allowedFields = [
            'first_name', 'last_name', 'middle_name', 'birthday', 'email', 'phone', 'school', 'city'
        ];

        if (Auth::guard('admin')->user()) {
            array_push($allowedFields, 'subscriptions_left');
        }

        $teacher->fill($data->only($allowedFields)->toArray());

        if ($data->has('password') && $data['password']) {
            $teacher->password = Hash::make($data['password']);
        }

        if ($data->has('image_id') && $data['image_id'] != ($teacher->image->id ?? null)) {
            $teacher->image()->delete();
            if ($data['image_id']) {
                $attachment = Attachment::findOrFail($data['image_id']);
                $teacher->image()->save($attachment);
            }
            $teacher->load('image');
        }

        $teacher->save();
        return $teacher;
    }

    public function giveTestSubscription(Teacher $teacher, $password = null): Teacher
    {
        $teacher->subscriptions_left = 5;
        $teacher->save();

        $subscription = new Subscription();
        $subscription->user()->associate($teacher);
        $subscription->expired_at = Carbon::now()->addMonth();
        $subscription->save();

        $subscriptionTransaction = new SubscriptionTransaction([
            'type' => 'accept',
            'description' => 'Получена пробная подписка (1 месяц доступа и 5 абонементов для учеников).'
        ]);

        $subscriptionTransaction->owner()->associate($teacher);
        $subscriptionTransaction->save();

        $teacher->notify(new TeacherTestSubscriptionNotification($password));

        return $teacher;
    }
}
