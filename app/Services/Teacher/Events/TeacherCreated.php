<?php


namespace App\Services\Teacher\Events;


use App\Models\Teacher;

class TeacherCreated
{
    /**
     * @var Teacher
     */
    private Teacher $teacher;

    public function __construct(Teacher $teacher)
    {
        $this->teacher = $teacher;
    }
}
