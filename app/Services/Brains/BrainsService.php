<?php


namespace App\Services\Brains;


use App\Models\BrainValue;
use Carbon\Carbon;

class BrainsService
{
    public function increase($studentId, $value)
    {
        $lastBrains = BrainValue::orderByDesc('id')->where('student_id', $studentId)->first();
        if ($lastBrains && Carbon::now()->isSameDay($lastBrains->date)) {
            $lastBrains->value += $value;
            $lastBrains->save();
            return $lastBrains->value;
        } else {
            BrainValue::create([
                'date' => Carbon::now(),
                'value' => ($lastBrains->value ?? 0) + $value,
                'student_id' => $studentId,
            ]);
            return $value;
        }
    }
}
