<?php


namespace App\Services\Feedback;


use App\Services\Variables\VariablesService;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class FeedbackService
{
    public function store(array $data)
    {
        Validator::validate($data, [
            'email' => 'required|string|max:30',
            'name' => 'required|string|max:30',
            'phone' => 'string|max:30',
            'message' => 'nullable|string|max:255',
        ]);
        $feedback = new Feedback($data['name'], $data['email'], $data['phone'], $data['message']);
        $notification = new FeedbackCreated($feedback);
        $email = app(VariablesService::class)->get('notificationEmail') ?? config('mail.admin-email');
        Notification::route('mail', $email)->notify($notification);
        return 1;
    }
}
