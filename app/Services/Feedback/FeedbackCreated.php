<?php


namespace App\Services\Feedback;


use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FeedbackCreated extends Notification
{
    private Feedback $feedback;

    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Новое сообщение')
            ->greeting('Новое сообщение в форме обратной связи!')
            ->line("**Имя**: " . $this->feedback->name)
            ->line("**Почта**: " . $this->feedback->email)
            ->line("**Телефон**: " . $this->feedback->phone)
            ->line("**Сообщение**: " . $this->feedback->message);
    }
}
