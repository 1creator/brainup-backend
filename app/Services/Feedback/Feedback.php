<?php


namespace App\Services\Feedback;


class Feedback
{
    public string $name;
    public string $phone;
    public ?string $email;
    public ?string $message;

    public function __construct($name, $email, $phone, $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->message = $message;
    }
}
