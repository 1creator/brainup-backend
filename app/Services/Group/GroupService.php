<?php


namespace App\Services\Group;


use App\Models\Group;
use App\Models\Schedule;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GroupService
{
    private function validateTime($attribute, $value, $fail)
    {

        if ($value === 'foo') {
            $fail($attribute . ' is invalid.');
        }
    }

    public function store(array $data): Group
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'name' => 'required|string|max:30',
            'description' => 'nullable|string',
            'schedules.*.start_at' => 'required',
            'schedules.*.finish_at' => 'required',
        ]);

        $group = new Group($data->only([
            'description', 'name'
        ])->toArray());
        $group->teacher_id = Auth::id();
        $group->save();

        if ($data->has('schedules')) {
            foreach ($data['schedules'] as $schedule) {
                Schedule::create(
                    [
                        'group_id' => $group['id'],
                        'date' => $schedule['date'],
                        'start_at' => $schedule['start_at'],
                        'finish_at' => $schedule['finish_at'],
                        'mon' => $schedule['mon'],
                        'tue' => $schedule['tue'],
                        'wed' => $schedule['wed'],
                        'thu' => $schedule['thu'],
                        'fri' => $schedule['fri'],
                        'sat' => $schedule['sat'],
                        'sun' => $schedule['sun'],
                    ]);
            }

            $group->load('schedules');
        }

        if ($data->has('student_ids')) {
            Student::whereIn('id', $data['student_ids'])->update(['group_id' => $group->id]);
        }

        return $group;
    }

    public function update(Group $group, array $data): Group
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'name' => 'required|string|max:30',
            'description' => 'nullable|string',
            'schedules.*.start_at' => 'required',
            'schedules.*.finish_at' => 'required',
        ]);

        $group->fill($data->only([
            'description', 'name'
        ])->toArray());

        /** @var Teacher $user */
        $user = Auth::user();

        if ($data->has('student_ids')) {
            $group->students()->whereNotIn('id', $data['student_ids'])->update(['group_id' => $user->defaultGroup->id]);
            Student::whereIn('id', $data['student_ids'])->update(['group_id' => $group->id]);
        }

        if ($data->has('schedules')) {
            foreach ($data['schedules'] as $schedule) {
                Schedule::updateOrCreate(
                    ['id' => $schedule['id'] ?? null],
                    [
                        'group_id' => $group['id'],
                        'date' => $schedule['date'],
                        'start_at' => $schedule['start_at'],
                        'finish_at' => $schedule['finish_at'],
                        'mon' => $schedule['mon'],
                        'tue' => $schedule['tue'],
                        'wed' => $schedule['wed'],
                        'thu' => $schedule['thu'],
                        'fri' => $schedule['fri'],
                        'sat' => $schedule['sat'],
                        'sun' => $schedule['sun'],
                    ]);
            }
            $group->load('schedules');
        }

        $group->save();
        return $group;
    }

    public function destroy(Group $group): bool
    {
        /** @var Teacher $user */
        $user = Auth::user();
        $group->students()->update(['group_id' => $user->defaultGroup->id]);
        return $group->delete() ? 1 : 0;
    }
}
