<?php

namespace App\Services\Attachment\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class AttachmentResource extends JsonResource
{

    public function toArray($request)
    {
        $url = $this->type != 'embed' ?
            Storage::disk($this->disk)->url($this->original) : $this->original;
        $thumbnail = $this->thumbnail ?
            Storage::disk($this->disk)->url($this->thumbnail) : null;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alt' => $this->alt,
            'type' => $this->type,
            'url' => $url,
            'thumbnail' => $thumbnail,
            'size' => $this->size,
            'order_column' => $this->order_column,
            'created_at' => $this->created_at,
        ];
    }
}
