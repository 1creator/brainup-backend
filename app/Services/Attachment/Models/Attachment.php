<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Services\Attachment\Models;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @method static findOrFail($image_id)
 */
class Attachment extends Model implements CastsAttributes
{
    const UPDATED_AT = null;
    protected $guarded = ['id'];
    protected $hidden = ['disk', 'original', 'attachable_id', 'attachable_type',];

    protected $casts = [
        'info' => 'array',
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
        if ($this->type != 'embed')
            return url(Storage::disk($this->disk)->url($this->attributes['original']));
        else return $this->attributes['original'];
    }

    public function attachable()
    {
        return $this->morphTo();
    }

    public function get($model, string $key, $value, array $attributes)
    {
        return static::find($value);
    }

    public function set($model, string $key, $value, array $attributes)
    {
        return $value->id;
    }
}
