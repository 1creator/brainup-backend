<?php


namespace App\Services\App;


use App\Models\AppSession;
use App\Models\FlashApp;
use App\Models\MultiplicationApp;
use App\Services\App\Rules\MultiplicationTemplateRule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class MultiplicationAppService implements BaseAppService
{
    protected int $trueAnswersForProgress = 5;

    public function createInstance($settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'operation' => 'required|in:mul,div',
            'task_count' => 'required|integer|min:1',
            'watch_time' => 'required|numeric|min:0.1',
            'input_time' => 'required|numeric',
            'is_integer' => 'required|boolean',
            'template1' => ['required', new MultiplicationTemplateRule(1)],
            'template2' => ['required', new MultiplicationTemplateRule(2)],
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'template1' => 'Шаблон 1',
            'template2' => 'Шаблон 2',
        ]);

        $appInstance = new MultiplicationApp($settings->toArray());
        $appInstance->save();
        return $appInstance;
    }

    public function updateInstance(Model $app, $settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'operation' => 'required|in:mul,div',
            'task_count' => 'required|integer|min:1',
            'watch_time' => 'required|numeric|min:0.1',
            'input_time' => 'required|numeric',
            'is_integer' => 'required|boolean',
            'template1' => ['required', new MultiplicationTemplateRule(1)],
            'template2' => ['required', new MultiplicationTemplateRule(2)],
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'template1' => 'Шаблон 1',
            'template2' => 'Шаблон 2',
        ]);

        $app->update($settings->toArray());
        return $app;
    }

    public function getTask(AppSession $appSession)
    {
        /** @var FlashApp $settings */
        $settings = $appSession->settings;

        // Если в сессии сгенерировано задач больше чем пользовательских ответов,
        // значит пользователь обновил страницу и нужно удалить задачи без ответов, что сгенерировать заново
        $appSession->flushTasks();

        // если в тренажере уже сгенерировано максимальное количество задач, заданное в конфигурации, то кидаем ошибку
        if ($appSession->isFinished()) {
            abort(404, "Тренажер уже закончен!");
        }

        $tasks = $appSession->tasks;
        $answers = $appSession->answers;

        [$numbers, $answer] = $this->getNumbersAndAnswer($appSession->settings);

        // этот массив уйдет пользователю
        $task = [
            'numbers' => $numbers,
            'watch_time' => $settings['watch_time'],
//            'answer' => $answer,
        ];

        $tasks[] = $task;
        $answers[] = $answer;

        $appSession->tasks = $tasks;
        $appSession->settings = $settings;
        $appSession->answers = $answers;

        if ($appSession->exists) {
            $appSession->save();
        }

        return $task;
    }

    public function getNumbersAndAnswer(array $settings): array
    {
        $numbers = [
            $this->makeNumberFromTemplate($settings['template1']),
            $this->makeNumberFromTemplate($settings['template2'], true),
        ];

        $answer = $settings['operation'] == 'mul'
            ? $numbers[0] * $numbers[1]
            : $numbers[0] / $numbers[1];

        if ($settings['is_integer'] && $settings['operation'] == 'div') {
            $answer = round($answer);
            $numbers[0] = $numbers[1] * $answer;
//            if ($numbers[0] == 0) {
//                $numbers[0] = $numbers[1];
//                $answer = 1;
//            }
        } else {
            $answer = round($answer, 2);
        }

        if (strlen(substr(strrchr($answer, "."), 1)) > 10) {
            $answer = round($answer, 2);
        }

        return [$numbers, $answer];
    }

    protected function makeNumberFromTemplate(string $template, $exceptNull = false)
    {
        for ($k = 0; $k < 10; $k++) {
            $res = '';
            for ($i = 0; $i < strlen($template); $i++) {
                switch ($template[$i]) {
                    case '#':
                        $res .= mt_rand(1, 9);
                        break;
                    case '$':
                        $res .= mt_rand(2, 9);
                        break;
                    case '*':
                        $res .= $exceptNull ? mt_rand(1, 9) : mt_rand(0, 9);
                        break;
                    case '.':
                        $res .= '.';
                        break;
                    default:
                        $res .= $template[$i];
                        break;
                }
            }
            $res = floatval($res);
            if (!$res) {
                continue;
            }
            return max($res, 1);
        }
    }

    public function getBrainsPerTask(AppSession $appSession)
    {
        /** @var FlashApp $settings */
        $settings = $appSession->settings;

        $sum1 = substr_count($settings['template1'], '#') + substr_count($settings['template1'], '*') + substr_count($settings['template1'], '$');
        $sum2 = substr_count($settings['template2'], '#') + substr_count($settings['template2'], '*') + substr_count($settings['template2'], '$');
        if (!$sum1 || !$sum2) {
            return 0;
        }
        return ($sum1 + $sum2) * 0.3;
    }
}
