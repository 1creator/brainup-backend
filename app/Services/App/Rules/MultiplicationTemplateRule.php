<?php


namespace App\Services\App\Rules;


use App\Models\Student;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class MultiplicationTemplateRule implements Rule
{
    private $index;

    public function __construct($index)
    {
        $this->index = $index;
    }

    public function passes($attribute, $value)
    {
        return preg_match('/^([0-9]|.|#|\$|\*)+$/', $value);
    }

    public function message()
    {
        return 'Задан некорректный шаблон множителя ' . ($this->index);
    }
}
