<?php

namespace App\Services\App\Enums;

class Formulas
{
    public const None = 'none';
    public const All = 'all';
    public const Five = 'five';
    public const Ten = 'ten';
    public const Mixed = 'mixed';
}
