<?php

namespace App\Services\App\Enums;

class AddictionOperations
{
    public const Sum = 'sum';
    public const Sub = 'sub';
    public const Mixed = 'mixed';
}
