<?php


namespace App\Services\App;


use App\Models\AppSession;
use App\Models\BaseApp;
use App\Models\Student;
use App\Services\Brains\BrainsService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AppSessionService
{
    public function play(BaseApp $app, $data)
    {
        $data = collect($data);

        /** @var Model $user */
        $user = Auth::user();

        $appSession = new AppSession([
            'settings' => $app->instance->toArray()
        ]);
        $appSession->app()->associate($app);

        if ($data->has('student_id')) {
            $appSession->player_type = Student::class;
            $appSession->player_id = $data['student_id'];
        } else {
            $appSession->player()->associate($user);
        }

        $appSession->save();
        $appSession->append('app_type');
        return $appSession;
    }

    public function getTaskList(BaseApp $app, $data)
    {
        $data = collect($data);
        $tasks = collect();
        $answers = collect();
        $appService = $app->getService();
        $appSession = new AppSession([
            'settings' => $app->instance->toArray()
        ]);
        for ($i = 0; $i < $data->get('count', 30); $i++) {
            $appService->getTask($appSession);
            $tasks = $tasks->merge($appSession->tasks);
            $answers = $answers->merge($appSession->answers);
        }

        $res = collect();
        switch ($app->type) {
            case 'static':
            case 'dynamic':
                $tasks->each(function ($item, $idx) use ($answers, $res) {
                    $formattedAddendums = array_map(fn($a) => $a >= 0 ? '+' . $a : $a, $item['addendums']);
                    $row = implode("   ", $formattedAddendums);
                    $row .= "    =    " . $answers[$idx];

                    $sum = 0;
                    foreach ($item['addendums'] as $addendum) {
                        $sum += (float)$addendum;
                        if ($sum < 0) {
                            $row .= "    Есть отрицательные";
                            break;
                        }

                    }

                    $res->push($row);
                });
                break;
            case 'multiplication':
                $operation = $appSession->settings['operation'] == 'mul' ? '*' : '/';
                $tasks->each(function ($item, $idx) use ($operation, $answers, $res) {
                    $formattedAddendums = array_map(fn($a) => $a >= 0 ? '' . $a : $a, $item['numbers']);
                    $row = implode("  " . $operation . " ", $formattedAddendums);
                    $row .= "    =    " . $answers[$idx];

                    $sum = 0;
                    foreach ($item['numbers'] as $addendum) {
                        $sum += (float)$addendum;
                        if ($sum < 0) {
                            $row .= "    Есть отрицательные";
                            break;
                        }

                    }

                    $res->push($row);
                });
                break;
            default:
                $res = [];
        }

        return $res;
    }

    public function restart(AppSession $appSession, $data)
    {
        $appSession->fill([
                'tasks' => [],
                'answers' => [],
                'state' => [],
                'quality' => 0,
                'finished' => 0,
            ]
        );
        $appSession->save();

        return $appSession;
    }

    public function updateSettings(AppSession $appSession, $data)
    {
        $instance = app('apps.' . $appSession->app_type)->createInstance($data['settings']);
        $appSession->settings = $instance->toArray();
        $appSession->save();
        return $appSession;
    }

    public function answer(AppSession $appSession, $data)
    {
        $state = $appSession->state;
        $state['user_answers'] = $state['user_answers'] ?? [];
        $currentAnswerIdx = $appSession->step;
        $state['user_answers'][$currentAnswerIdx] = $data['answer'];
        $appSession->state = $state;
        $trueAnswer = $appSession->answers[$currentAnswerIdx];

        if ($appSession->isFinished()) {
            $appSession->finished = true;
        }

        $appSession->quality = $appSession->calcQuality();
        $appSession->save();

        $nextTaskIndex = $appSession->finished ? null : $appSession->step;

        $result = $data['answer'] !== '' && ($trueAnswer == $data['answer'] ? 1 : 0);

        if ($result && $appSession->player_type == Student::class) {
            app(BrainsService::class)->increase($appSession->player_id, $appSession->brainsPerTask);
        }

        return [
            'result' => $result,
            'answer' => $trueAnswer,
            'quality' => $appSession->quality,
            'next_task_index' => $nextTaskIndex,
        ];
    }

    public function getTask(AppSession $appSession)
    {
        return $appSession->app->getService()->getTask($appSession);
    }
}
