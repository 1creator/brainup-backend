<?php


namespace App\Services\App;


use App\Models\AppSession;
use App\Models\DynamicApp;
use App\Services\App\Enums\AddictionOperations;
use App\Services\App\Enums\Formulas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DynamicAppService implements BaseAppService
{
    protected int $trueAnswersForProgress = 5;

    public function createInstance($settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'operations' => 'required|in:sum,sub,mixed',
            'operation_alt' => 'required|boolean',
            'bit_alt' => 'required|boolean',
            'bit_formulas' => 'required|in:all,except_up',
            'formulas' => 'required|in:none,all,five,ten,mixed',
            'digits_capacity' => 'required|array',
            'fraction' => 'required|integer|min:0',
            'negative' => 'required|boolean',
            'mirror' => 'required|boolean',
            'task_count' => 'required|integer|min:1',
            'task_size' => 'required|integer|min:2',
            'task_size_progress' => 'required|boolean',
            'watch_time' => 'required|numeric|min:0.1',
            'watch_time_progress' => 'required|boolean',
            'min_watch_time' => 'required_if:watch_time_progress,true|numeric|min:0.1',
            'input_time' => 'required|numeric'
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'min_watch_time' => 'Минимальное время',
        ]);

        $appInstance = new DynamicApp($settings->toArray());
        $appInstance->save();
        return $appInstance;
    }

    public function updateInstance(Model $app, $settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'operations' => 'required|in:sum,sub,mixed',
            'operation_alt' => 'required|boolean',
            'bit_alt' => 'required|boolean',
            'bit_formulas' => 'required|in:all,except_up',
            'formulas' => 'required|in:none,all,five,ten,mixed',
            'digits_capacity' => 'required|array',
            'fraction' => 'required|integer|min:0',
            'negative' => 'required|boolean',
            'mirror' => 'required|boolean',
            'task_count' => 'required|integer|min:1',
            'task_size' => 'required|integer|min:2',
            'task_size_progress' => 'required|boolean',
            'watch_time' => 'required|numeric|min:0.1',
            'watch_time_progress' => 'required|boolean',
            'min_watch_time' => 'required_if:watch_time_progress,true|numeric|min:0.1',
            'input_time' => 'required|numeric',
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'min_watch_time' => 'Минимальное время',
        ]);

        $app->update($settings->toArray());
        return $app;
    }

    public function getTask(AppSession $appSession): array
    {
        /** @var DynamicApp $settings */
        $settings = $appSession->settings;

        // Если в сессии сгенерировано задач больше чем пользовательских ответов,
        // значит пользователь обновил страницу и нужно удалить задачи без ответов, что сгенерировать заново
        $appSession->flushTasks();

        // если в тренажере уже сгенерировано максимальное количество задач, заданное в конфигурации, то кидаем ошибку
        if ($appSession->isFinished()) {
            abort(404, "Тренажер уже закончен!");
        }

        $tasks = $appSession->tasks;
        $answers = $appSession->answers;

        $watchTimeChange = $this->getWatchTimeChange($appSession);
        $taskSizeChange = $this->getTaskSizeChange($appSession);

        if ($watchTimeChange && $taskSizeChange) {
            switch ($this->resolveWatchTimeTaskSizeConflict($appSession)) {
                case 'watch_time':
                    $taskSizeChange = 0;
                    break;
                case 'task_size':
                    $watchTimeChange = 0;
                    break;
            }
        }

        $settings['watch_time'] = round($settings['watch_time'] + $watchTimeChange, 2);
        $settings['task_size'] = round($settings['task_size'] + $taskSizeChange, 2);;

        // массив слагаемых и ответ
        [$addendumArr, $answer] = $this->getAddendumsAndAnswer($appSession->settings);

        // этот массив уйдет пользователю
        $task = [
            'addendums' => $addendumArr,
            'watch_time' => $settings['watch_time'],
//            'answer' => $answer,
        ];

        $tasks[] = $task;
        $answers[] = $answer;

        $appSession->tasks = $tasks;
        $appSession->settings = $settings;
        $appSession->answers = $answers;

        if ($appSession->exists) {
            $appSession->save();
        }

        return $task;
    }

    public function getAddendumsAndAnswer($settings): array
    {
        $addendumArr = [];
        $sum = 0;

        // помещаем в массив task_size слагаемых
        for ($i = 0; $i < $settings['task_size']; $i++) {
            $addendum = $this->makeAddendum(
                $settings,
                $this->sumArray($addendumArr),
                $addendumArr[$i - 1] ?? null
            );
            $addendumArr[] = $addendum;
            $sum += floatval($addendum);
        }

        $sum = round($sum, $settings['fraction']);

        return [$addendumArr, $sum];
    }

    public function makeAddendum(array $settings, $sum, $prevAddendum)
    {
        $sign = $this->getSign($settings, $prevAddendum, $sum);

        // $requiredCapacity - количество разрядов генерируемого слагаемого
        $requiredCapacity = $this->getRequiredCapacity($settings, $prevAddendum);
        $requiredCapacityCopy = $requiredCapacity;

        // если нужны зеркальные числа, то сгенерируем число разрядностью 1 и позже его скопируем на другие разряды
        if ($settings['mirror']) {
            $requiredCapacity = 1;
        }

        $formula = $settings['formulas'] == Formulas::All
            ? collect([Formulas::Mixed, Formulas::Ten])->random()
            : $settings['formulas'];

        $changedSign = false;

        do {
            if (!$sign && ($this->getDigitsCapacity($sum) < $requiredCapacity)) {
                // если текущая разрядность меньше разрядности генерируемого слагаемого, то принудительно складываем
                $sign = true;
            }

            /*
             * если запрещены отрицательные числа и сейчас отрицание,
             * то ограничиваем количество подбираемых цифр не более количества цифр в сумме
            */
            if (!$settings['negative'] && !$sign) {
                $requiredCapacity = min($this->getDigitsCapacity($sum), $requiredCapacity);
            }

            $addendum = $this->generateAddendumNumber(
                $sum,
                $requiredCapacity,
                $sign,
                $formula,
                $prevAddendum,
                $settings['negative'],
            );
            // если не получилось подобрать слагаемое, то сменим знак и попробуем подобрать еще раз
            if (!$addendum) {
                if (!$changedSign && $settings['operations'] === AddictionOperations::Mixed && !$settings['operation_alt']) {
                    Log::info('could not found addendum, changing sign to ' . ($sign ? '-' : '+'));
                    $sign = !$sign;
                    $changedSign = true;
                } else {
                    // если уже поменяли знак или не можем его менять, то упрощаем формулу
                    $formula = $this->getEasierFormula($formula);
                    Log::info('made formula simpler: ' . $formula);
                    $changedSign = false;
                    if (!$formula) {
                        $addendum = -$sum;
                        break;
                    }
                }
            }
        } while (!$addendum);

        if ($settings['mirror']) {
            // если нужны зеркальные числа, то склонируем сгенерированное число на другие разряды
            $digit = $addendum;
            $addendum = 0;


            if (!$settings['negative'] && $digit < 0) {
                $requiredCapacityCopy = min($this->getDigitsCapacity($sum), $requiredCapacity);
            }

            for ($k = $requiredCapacityCopy; $k > 0; $k--) {
                $addendum += $digit * pow(10, $k - 1);
            }
        }

        if ($settings['fraction']) {
            $addendum /= pow(10, $settings['fraction']);
        }

        $addendum = round($addendum, $settings['fraction']);

        $mode = $settings['mode'] ?? null;
        if ($mode == 'combinations' || ($mode == 'mixed' && rand(0, 1))) {
            $addendum = $addendum . "'";
        }

        return $addendum;
    }

    public function generateAddendumNumber($sum, $requiredCapacity, $sign = true,
                                           $formula = Formulas::All,
                                           $prev = 0, $allowNegative = false
    )
    {
        // если это первое слагаемое, то просто генерируем число заданной разрядности
        /*if (!$sum) {
            return random_int(pow(10, $requiredCapacity - 1), pow(10, $requiredCapacity) - 1);
        } else*/

//        $path = $this->getFormulaPath($formula);

        // $path - цепочка формул для поиска слагаемого
        $digits = collect();

//            if ($path[0] == 'inverse') {
//                array_shift($path);
//                $sign = !$sign;
//            }


        $sumNormalized = $this->normalizeNumberCapacity($sum, $requiredCapacity);
        $prevNormalized = $this->normalizeNumberCapacity($prev, $requiredCapacity);

        Log::info("\n\n");
        Log::info('$sumNormalized: ' . $sumNormalized);
        Log::info('$requiredCapacity: ' . $requiredCapacity);

        // справа налево по разрядам
        for ($i = 0; $i < $requiredCapacity; $i++) {
//                Log::info('$sign: ' . ($sign ? '+' : '-'));
            $prevDigit = (int)$prevNormalized[-1 - $i];
            $sumDigit = (int)$sumNormalized[-1 - $i];

            // запрещаем отмену предыдущего (+1-1, +2-2, -2+2, -3+3)
            $except = $sign != ($prev > 0)
                ? [$prevDigit]
                : [];

            /*
             * если запрещены отрицательные результаты
             * и сейчас отрицание
             * и сейчас последний разряд или первый (с условием, что забрали единицу на последнем разряде)
             * то исключаем цифры больше текущего разряда, чтобы не уйти в минус
             * */
            if (!$allowNegative && !$sign && ($i == $this->getDigitsCapacity($sum) - 1)) {
                Log::info('sum capacity: ' . $this->getDigitsCapacity($sum));
                Log::info('excepting: ' . json_encode(range($sumDigit, 9)));
                array_push($except, ...range($sumDigit, 9));
            }

            $variants = $this->makeVariants($formula, $sumDigit, $sign, $except);
            Log::info('$variants: ' . json_encode($variants));

            if (!$variants->isEmpty()) {
                $digits->prepend($variants->random());
            } else {
                $digits->prepend(0);
            }
            Log::info('$digits: ' . $digits);
        }

//            array_shift($path);
        //            if (!$number) {
//                $sign = !$sign;
//                Log::info('shift formula');
//            }
//        Log::info('$number: ' . $number);

        return (int)$digits->join('') * ($sign ? 1 : -1);
    }

    public function makeVariants($formula, $firstDigit, $sign, $except): Collection
    {
        switch ($formula) {
            case Formulas::None:
                $variants = $this->makeSimpleDigitVariants($firstDigit, $sign);
                break;
            case Formulas::Five:
                $variants = $this->makeDigitVariantsIn5($firstDigit, $sign);
                break;
            case Formulas::Ten:
                $variants = $this->makeDigitVariantsIn10($firstDigit, $sign);
                break;
            case Formulas::Mixed:
                $variants = $this->makeMixedDigitVariants($firstDigit, $sign);
                break;
            default:
                $variants = collect();
        }

        return $variants->reject(fn($item) => in_array($item, $except));
    }

    private function getEasierFormula($formula)
    {
        $formulas = [
            Formulas::Mixed => Formulas::Ten,
            Formulas::Ten => Formulas::Five,
            Formulas::Five => Formulas::None,
        ];
        return $formulas[$formula] ?? null;
    }

    protected function makeSimpleDigitVariants($prev = 0, $sign = true): Collection
    {
        if ($sign) {
            return collect([
                0 => [1, 2, 3, 4, 6, 7, 8, 9],
                1 => [1, 2, 3, 5, 6, 7, 8],
                2 => [1, 2, 5, 6, 7],
                3 => [1, 5, 6],
                4 => [5],
                5 => [1, 2, 3, 4],
                6 => [1, 2, 3],
                7 => [1, 2],
                8 => [1],
                9 => [],
            ][$prev]);
        } else {
            return collect([
                0 => [],
                1 => [],
                2 => [1,],
                3 => [1, 2,],
                4 => [1, 2, 3,],
                5 => [],
                6 => [1, 5,],
                7 => [1, 2, 5, 6,],
                8 => [1, 2, 3, 5, 6, 7,],
                9 => [1, 2, 3, 4, 5, 6, 7, 8,]
            ][$prev]);
        }
    }

    protected function makeDigitVariantsIn5($prev = 0, $sign = true): Collection
    {
        if ($sign) {
            return collect([
                0 => [],
                1 => [4],
                2 => [3, 4],
                3 => [2, 3, 4],
                4 => [1, 2, 3, 4],
                5 => [],
                6 => [],
                7 => [],
                8 => [],
                9 => []
            ][$prev]);
        } else {
            return collect([
                0 => [],
                1 => [],
                2 => [],
                3 => [],
                4 => [],
                5 => [1, 2, 3, 4],
                6 => [2, 3, 4],
                7 => [3, 4],
                8 => [4],
                9 => [],
            ][$prev]);
        }
    }

    protected function makeDigitVariantsIn10($prev = 0, $sign = true): Collection
    {
        if ($sign) {
            return collect([
                0 => [],
                1 => [9],
                2 => [8, 9],
                3 => [7, 8, 9],
                4 => [6, 7, 8, 9],
                5 => [5],
                6 => [4, 5, 9],
                7 => [3, 4, 5, 8, 9],
                8 => [2, 3, 4, 5, 7, 8, 9],
                9 => [1, 2, 3, 4, 5, 6, 7, 8],
            ][$prev]);
        } else {
            return collect([
                0 => [1, 2, 3, 4, 5, 6, 7, 8, 9],
                1 => [2, 3, 4, 5, 7, 8, 9],
                2 => [3, 4, 5, 8, 9],
                3 => [4, 5, 9],
                4 => [5],
                5 => [6, 7, 8, 9],
                6 => [7, 8, 9],
                7 => [8, 9],
                8 => [9],
                9 => [],
            ][$prev]);
        }
    }

    protected function makeMixedDigitVariants($prev = 0, $sign = true): Collection
    {
        if ($sign) {
            return collect([
                0 => [],
                1 => [],
                2 => [],
                3 => [],
                4 => [],
                5 => [6, 7, 8, 9],
                6 => [6, 7, 8],
                7 => [6, 7],
                8 => [6],
                9 => [],
            ][$prev]);
        } else {
            return collect([
                0 => [],
                1 => [6],
                2 => [6, 7],
                3 => [6, 7, 8],
                4 => [6, 7, 8, 9],
                5 => [],
                6 => [],
                7 => [],
                8 => [],
                9 => [],
            ][$prev]);
        }
    }

    protected function getWatchTimeChange(AppSession $appSession)
    {
        /** @var DynamicApp $settings */
        $settings = $appSession->settings;
        $userAnswers = $appSession->state['user_answers'] ?? [];

        if (!$settings['watch_time_progress'] || count($userAnswers) < $this->trueAnswersForProgress) {
            return 0;
        }

        $lastWatchTime = $appSession->tasks[count($appSession->tasks) - 1]['watch_time'];

        if ($this->isProgressedLastTasks($appSession, $this->trueAnswersForProgress)) {
            return 0;
        }

        $rightAnswersCount = $this->countTrueAnswers($appSession, $this->trueAnswersForProgress);

        if ($rightAnswersCount == 0) {
            return 0.1;
        } elseif ($rightAnswersCount == $this->trueAnswersForProgress && $lastWatchTime > $appSession->min_watch_time) {
            return -0.1;
        }

        return 0;
    }

    protected function getTaskSizeChange(AppSession $appSession): int
    {
        /** @var DynamicApp $settings */
        $settings = $appSession->settings;
        $userAnswers = $appSession->state['user_answers'] ?? [];

        if (!$settings['task_size_progress'] || count($userAnswers) < $this->trueAnswersForProgress) {
            return 0;
        }

        $lastTaskSize = count($appSession->tasks[count($appSession->tasks) - 1]['addendums']);

        if ($this->isProgressedLastTasks($appSession, $this->trueAnswersForProgress)) {
            return 0;
        }

        $rightAnswersCount = $this->countTrueAnswers($appSession, $this->trueAnswersForProgress);

        if ($rightAnswersCount == 0 && $lastTaskSize > 3) {
            return -1;
        } elseif ($rightAnswersCount == $this->trueAnswersForProgress) {
            return 1;
        }

        return 0;
    }

    protected function isProgressedLastTasks(AppSession $appSession, int $count): bool
    {
        $last5WatchTime = array_map(fn($item) => $item['watch_time'], array_slice($appSession->tasks, -$count));
        $isLast5WatchTimeEquals = count(array_unique($last5WatchTime)) == 1;

        $last5TaskSize = array_map(fn($item) => count($item['addendums']), array_slice($appSession->tasks, -$count));
        $isLast5TaskSizeEquals = count(array_unique($last5TaskSize)) == 1;

        return !$isLast5WatchTimeEquals || !$isLast5TaskSizeEquals;
    }

    protected function getSign($settings, $prev, $sum): bool
    {
        if ($settings['operations'] == AddictionOperations::Sub) {
            // если используем только операцию вычитания, то делаем слагаемое отрицательным
            if ($settings['negative']) {
                return false;
            } else {
                return !$prev;
            }
        }

        if (!$settings['negative'] && $sum <= 0) {
            return true;
        }

        if ($settings['operations'] == AddictionOperations::Mixed) {
            // если используем смешанные операции
            if ($settings['operation_alt'] && $prev) {
                // если используем чередование операций и это не первое слогаемое,
                // то смотрим предыдущее слагамое и делаем другой знак
                return !($prev > 0);
            } else {
                // если не используем чередование операций, то просто делаем рандомный знак числа
                return (bool)mt_rand(0, 1);
            }
        }

        return true;
    }

    protected function getRequiredCapacity($settings, $prev)
    {
        if ($settings['bit_alt'] && ($settings['digits_capacity'][1] - $settings['digits_capacity'][0] > 0)) {
            // если стоит опция чередования разрядности слагаемого
            // если это первое слагаемое, то генерируем слагаемое максимально допустимой разрядности
            if (!$prev) {
                return $settings['digits_capacity'][1] + $settings['fraction'];
            } else {
                // если это не первое слагаемое, то генерируем слагаемое разрядностью на один больше предыдущего
                // (или минимальная разрядность, если достигли максимальной)
                $previousCapacity = $this->getDigitsCapacity((int)$prev);
//                Log::info("previousCapacity: $previousCapacity, prev: $prev");
                if ($previousCapacity + 1 <= $settings['digits_capacity'][1]) {
                    return $previousCapacity + 1 + $settings['fraction'];
                } else {
                    return $settings['digits_capacity'][0] + $settings['fraction'];
                }
            }
        } else {
            // иначе просто генерируем случайную допустимую разрядность
            return mt_rand($settings['digits_capacity'][0], $settings['digits_capacity'][1]) + $settings['fraction'];
        }
    }

    protected function countTrueAnswers(AppSession $appSession, $taskCount): int
    {
        $last5UserAnswers = array_slice($appSession->state['user_answers'] ?? [], -$taskCount);
        $last5TrueAnswers = array_slice($appSession->answers, -$taskCount);
        return count(array_intersect_assoc($last5TrueAnswers, $last5UserAnswers));
    }

    protected function normalizeNumberCapacity($number, $capacity): string
    {
        $explodedNumber = explode('.', abs($number));
        $intAbsNumber = $explodedNumber[0]; // нормализуем только целую часть

        $capacityDiff = strlen($intAbsNumber) - $capacity; // разница в количестве символов целой части
        if ($capacityDiff < 0) { // если не хватает разрядов
            $intAbsNumber = str_repeat('0', abs($capacityDiff)) . $intAbsNumber; // добавляем нули слева
        }

        return ($number > 0 ? '+' : '-') . $intAbsNumber . ($explodedNumber[1] ?? '');
    }

    /**
     * @param $val
     * @return false|float|int
     *
     * Функция считает количество разрядов в числе
     */
    protected function getDigitsCapacity($val)
    {
        $val = (int)$val;
        if ($val === 0) {
            return 1;
        }
        $val = (string)abs($val);
        $val = str_replace('.', '', $val);
        return $val == 1 ? 1 : ceil(log10($val));
    }

    protected function sumArray($arr): float
    {
        return array_reduce($arr, fn($acc, $item) => $acc + (float)$item, 0);
    }

    protected function resolveWatchTimeTaskSizeConflict(AppSession $appSession): string
    {
        if (count($appSession->tasks) <= $this->trueAnswersForProgress) return 'watch_time';

        $taskBeforePrevProgress = $appSession->tasks[count($appSession->tasks) - $this->trueAnswersForProgress - 1];
        $lastTask = $appSession->tasks[count($appSession->tasks) - 1];

        if ($taskBeforePrevProgress['watch_time'] == $lastTask['watch_time']) {
            return 'watch_time';
        } else {
            return 'task_size';
        }
    }

    public function getBrainsPerTask(AppSession $appSession)
    {
        /** @var DynamicApp $settings */
        $settings = $appSession->settings;
        $l = [
            'none' => 0.3,
            'five' => 0.45,
            'ten' => 0.6,
            'all' => 0.5,
            'mixed' => 0.7,
        ][$settings['formulas']];
        $k = 1;
        if ($settings['mirror']) {
            $k += 2;
        }
        $d = $settings['fraction'] + ($settings['digits_capacity'][0] + $settings['digits_capacity'][1]) / 2;
        return $settings['task_size'] * $l * $d / ($settings['watch_time'] * $k);
    }
}
