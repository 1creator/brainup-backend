<?php


namespace App\Services\App;


use App\Models\AppSession;
use App\Models\FlashApp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class FlashAppService implements BaseAppService
{
    protected int $trueAnswersForProgress = 5;

    public function createInstance($settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'digits_capacity' => 'required|array',
            'task_count' => 'required|integer|min:1',
            'watch_time_progress' => 'required|boolean',
            'watch_time' => 'required|numeric|min:0.1',
            'min_watch_time' => 'required_if:watch_time_progress,true|numeric|min:0.1',
            'input_time' => 'required|numeric',
            'task_size' => 'required|numeric|min:1|max:5',
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'min_watch_time' => 'Минимальное время',
        ]);
        $appInstance = new FlashApp($settings->toArray());
        $appInstance->save();
        return $appInstance;
    }

    public function updateInstance(Model $app, $settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'digits_capacity' => 'required|array',
            'task_count' => 'required|integer|min:1',
            'watch_time' => 'required|numeric|min:0.1',
            'watch_time_progress' => 'required|boolean',
            'min_watch_time' => 'required_if:watch_time_progress,true|numeric|min:0.1',
            'input_time' => 'required|numeric',
            'task_size' => 'required|numeric|min:1|max:5',
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'min_watch_time' => 'Минимальное время',
        ]);

        $app->update($settings->toArray());
        return $app;
    }

    public function getTask(AppSession $appSession)
    {
        /** @var FlashApp $settings */
        $settings = $appSession->settings;

        // Если в сессии сгенерировано задач больше чем пользовательских ответов,
        // значит пользователь обновил страницу и нужно удалить задачи без ответов, что сгенерировать заново
        $appSession->flushTasks();

        // если в тренажере уже сгенерировано максимальное количество задач, заданное в конфигурации, то кидаем ошибку
        if ($appSession->isFinished()) {
            abort(404, "Тренажер уже закончен!");
        }

        $tasks = $appSession->tasks;
        $answers = $appSession->answers;

        $watchTimeChange = $this->getWatchTimeChange($appSession);
        $settings['watch_time'] = round($settings['watch_time'] + $watchTimeChange, 2);

        $numbers = [];

        for ($i = 0; $i < $settings['task_size']; $i++) {
            $numbers[] = $this->makeNumber($appSession);
        }

        // этот массив уйдет пользователю
        $task = [
            'number' => $numbers,
            'watch_time' => $settings['watch_time'],
        ];

        $tasks[] = $task;
        $answers[] = implode(';', $numbers);

        $appSession->tasks = $tasks;
        $appSession->settings = $settings;
        $appSession->answers = $answers;

        if ($appSession->exists) {
            $appSession->save();
        }

        return $task;
    }

    protected function makeNumber(AppSession $appSession)
    {
        /** @var FlashApp $settings */
        $settings = $appSession->settings;
        $requiredCapacity = mt_rand($settings['digits_capacity'][0], $settings['digits_capacity'][1]);

        return mt_rand(pow(10, $requiredCapacity - 1) + 1, pow(10, $requiredCapacity) - 1);
    }

    protected function getWatchTimeChange(AppSession $appSession)
    {
        /** @var FlashApp $settings */
        $settings = $appSession->settings;
        $userAnswers = $appSession->state['user_answers'] ?? [];

        if (!$settings['watch_time_progress'] || count($userAnswers) < $this->trueAnswersForProgress) {
            return 0;
        }

        $lastWatchTime = $appSession->tasks[count($appSession->tasks) - 1]['watch_time'];

        if ($this->isProgressedLastTasks($appSession, $this->trueAnswersForProgress)) {
            return 0;
        }

        $rightAnswersCount = $this->countTrueAnswers($appSession, $this->trueAnswersForProgress);

        if ($rightAnswersCount == 0) {
            return 0.1;
        } elseif ($rightAnswersCount == $this->trueAnswersForProgress && $lastWatchTime > $appSession->min_watch_time) {
            return -0.1;
        }

        return 0;
    }

    protected function isProgressedLastTasks(AppSession $appSession, int $count)
    {
        $last5WatchTime = array_map(fn($item) => $item['watch_time'], array_slice($appSession->tasks, -$count));
        $isLast5WatchTimeEquals = count(array_unique($last5WatchTime)) == 1;

        return !$isLast5WatchTimeEquals;
    }

    protected function countTrueAnswers(AppSession $appSession, $taskCount)
    {
        $last5UserAnswers = array_slice($appSession->state['user_answers'] ?? [], -$taskCount);
        $last5TrueAnswers = array_slice($appSession->answers, -$taskCount);
        return count(array_intersect_assoc($last5TrueAnswers, $last5UserAnswers));
    }

    public function getBrainsPerTask(AppSession $appSession)
    {
        /** @var FlashApp $settings */
        $settings = $appSession->settings;

        $avgCapacity = ($settings['digits_capacity'][0] + $settings['digits_capacity'][1]) / 2;

        return $avgCapacity * 0.3 / ($settings['watch_time'] + $settings['input_time'] * 0.3);
    }
}
