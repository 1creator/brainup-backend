<?php


namespace App\Services\App;


use App\Models\BaseApp;
use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AppService
{
    public function store(array $data): BaseApp
    {
        $data = collect($data);

        Validator::validate($data->toArray(), [
            'type' => 'required|in:dynamic,static,flash,multiplication'
        ]);

        $instance = app('apps.' . $data['type'])->createInstance($data['settings']);

        $data['instance_id'] = $instance->id;
        $data['instance_type'] = get_class($instance);

        if (!$data['name']) {
            $data['name'] = __($data['type']);
        }

        $app = new BaseApp($data->only([
            'name', 'instance_id', 'instance_type', 'is_homework', 'min_quality',
        ])->toArray());
        $app->invite_token = Str::random();

        /** @var Model $user */
        $user = Auth::user();
        $app->author()->associate($user);

        $app->save();

        if ($data->has('student_ids')) {
            $app->students()->sync($data['student_ids']);
        }

        if ($user instanceof Student) {
            $app->students()->save($user);
        }

        return $app;
    }

    public function update(BaseApp $app, array $data): BaseApp
    {
        $data = collect($data);

        $app->fill($data->only([
            'name', 'is_homework', 'min_quality',
        ])->toArray());

        $app->getService()->updateInstance($app->instance, $data['settings']);

        $app->save();

        if ($data->has('student_ids')) {
            $app->students()->sync($data['student_ids']);
        }
        return $app;
    }

    public function destroy(BaseApp $app): bool
    {
        return $app->delete() ? 1 : 0;
    }
}
