<?php


namespace App\Services\App;


use App\Models\AppSession;
use Illuminate\Database\Eloquent\Model;

interface BaseAppService
{
    public function createInstance($settings): Model;

    public function updateInstance(Model $app, $settings): Model;

    public function getTask(AppSession $appSession);

    public function getBrainsPerTask(AppSession $appSession);
}
