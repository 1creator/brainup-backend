<?php


namespace App\Services\App;


use App\Models\AppSession;
use App\Models\StaticApp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class StaticAppService extends DynamicAppService
{
    public function createInstance($settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'operations' => 'required|in:sum,sub,mixed',
            'operation_alt' => 'required|boolean',
            'bit_alt' => 'required|boolean',
            'bit_formulas' => 'required|in:all,except_up',
            'formulas' => 'required|in:none,all,five,ten,mixed',
            'digits_capacity' => 'required|array',
            'fraction' => 'required|integer|min:0',
            'negative' => 'required|boolean',
            'horizontal' => 'boolean',
            'mirror' => 'required|boolean',
            'task_count' => 'required|integer|min:1',
            'task_size' => 'required|integer|min:2',
            'task_size_progress' => 'required|boolean',
            'watch_time' => 'required|numeric|min:0.1',
            'watch_time_progress' => 'required|boolean',
            'min_watch_time' => 'required_if:watch_time_progress,true|numeric|min:0.1',
            'input_time' => 'required|numeric'
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'min_watch_time' => 'Минимальное время',
        ]);

        $appInstance = new StaticApp($settings->toArray());
        $appInstance->save();
        return $appInstance;
    }

    public function updateInstance(Model $app, $settings): Model
    {
        $settings = collect($settings);
        Validator::validate($settings->toArray(), [
            'operations' => 'required|in:sum,sub,mixed',
            'operation_alt' => 'required|boolean',
            'bit_alt' => 'required|boolean',
            'bit_formulas' => 'required|in:all,except_up',
            'formulas' => 'required|in:none,all,five,ten,mixed',
            'digits_capacity' => 'required|array',
            'fraction' => 'required|integer|min:0',
            'negative' => 'required|boolean',
            'horizontal' => 'boolean',
            'mirror' => 'required|boolean',
            'task_count' => 'required|integer|min:1',
            'task_size' => 'required|integer|min:2',
            'task_size_progress' => 'required|boolean',
            'watch_time' => 'required|numeric|min:1',
            'watch_time_progress' => 'required|boolean',
            'min_watch_time' => 'required_if:watch_time_progress,true|numeric|min:0.1',
            'input_time' => 'required|numeric',
        ], [], [
            'task_count' => 'Количество примеров',
            'task_size' => 'Слагаемых в примере',
            'watch_time' => 'Время на просмотр',
            'input_time' => 'Время на ввод ответа',
            'fraction' => 'Знаков после запятой',
            'min_watch_time' => 'Минимальное время',
        ]);

        $app->update($settings->toArray());
        return $app;
    }

    public function getTask(AppSession $appSession): array
    {
        /** @var StaticApp $settings */
        $settings = $appSession->settings;

        // Если в сессии сгенерировано задач больше чем пользовательских ответов,
        // значит пользователь обновил страницу и нужно удалить задачи без ответов, что сгенерировать заново
        $appSession->flushTasks();

        // если в тренажере уже сгенерировано максимальное количество задач, заданное в конфигурации, то кидаем ошибку
        if ($appSession->isFinished()) {
            abort(404, "Тренажер уже закончен!");
        }

        $tasks = $appSession->tasks;
        $answers = $appSession->answers;


        $watchTimeChange = $this->getWatchTimeChange($appSession);
        $taskSizeChange = $this->getTaskSizeChange($appSession);

        if ($watchTimeChange && $taskSizeChange) {
            switch ($this->resolveWatchTimeTaskSizeConflict($appSession)) {
                case 'watch_time':
                    $taskSizeChange = 0;
                    break;
                case 'task_size':
                    $watchTimeChange = 0;
                    break;
            }
        }

        $settings['watch_time'] = round($settings['watch_time'] + $watchTimeChange, 2);
        $settings['task_size'] = round($settings['task_size'] + $taskSizeChange, 2);;

        $settings['mode'] = null;
        // массив слагаемых и ответ
        [$addendumArr, $answer] = $this->getAddendumsAndAnswer($settings);

        // этот массив уйдет пользователю
        $task = [
            'addendums' => $addendumArr,
            'watch_time' => $settings['watch_time'],
//            'answer' => $answer,
        ];

        // если режим "Бусины" или "Смешанный" и четный пример, то устанавливаем флажок
//        $task['combinations'] = ($settings['mode'] == 'combinations') || ($settings['mode'] == 'mixed' && ($tasks % 2));

        $tasks[] = $task;
        $answers[] = $answer;

        $appSession->tasks = $tasks;
        $appSession->settings = $settings;
        $appSession->answers = $answers;

        if ($appSession->exists) {
            $appSession->save();
        }

        return $task;
    }

    public function getBrainsPerTask(AppSession $appSession)
    {
        /** @var StaticApp $settings */
        $settings = $appSession->settings;
        $l = [
            'none' => 0.3,
            'five' => 0.45,
            'ten' => 0.6,
            'all' => 0.5,
            'mixed' => 0.7,
        ][$settings['formulas']];
        $k = 1;
        if ($settings['mirror']) {
            $k += 2;
        }
        $d = $settings['fraction'] + ($settings['digits_capacity'][0] + $settings['digits_capacity'][1]) / 2;
        return $settings['task_size'] * $l * $d / (($settings['watch_time'] / 2) * $k);
    }
}
