<?php


namespace App\Services\ResetPassword\Services;


use App\Services\ResetPassword\Notifications\ResetPasswordNotification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ResetPasswordService
{
    const RESET_WITH_EMAIL = 'email';
    const RESET_WITH_PHONE = 'phone';

    public function send(array $data, $modelClass)
    {
        $identifyField = array_key_exists('email', $data) ?
            ResetPasswordService::RESET_WITH_EMAIL : ResetPasswordService::RESET_WITH_PHONE;

        Validator::validate($data, [
            'email' => 'required_without:phone|email|exists:' . $modelClass,
            'phone' => 'required_without:email|regex:/^((7)+([0-9]){10})$/|exists:' .$modelClass,
        ]);
        $user = $modelClass::where($identifyField, $data[$identifyField])->firstOrFail();

        $code = rand(100000, 999999);
        $cacheKey = $this->getCacheKey($user);
        Cache::put($cacheKey, [
            $code, get_class($user), $user->getKey()
        ], 300);

        Notification::send($user, new ResetPasswordNotification($identifyField, $code));
        return $cacheKey;
    }

    public function reset(array $data)
    {
        Validator::validate($data, [
            'code' => 'required',
            'token' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $value = Cache::get($data['token']);
        $model = null;
        if ($value && $value[0] == $data['code']) {
            $model = (new $value[1])->findOrFail($value[2]);
        } else {
            throw ValidationException::withMessages(['code' => 'Неправильный код подтверждения']);
        }

        $password = Hash::make($data['password']);
        $model['password'] = $password;
        $model->save();

        return $model;
    }

    private function getCacheKey($user): string
    {
        return md5('reset_password.' . get_class($user) . '.' . $user['id']);
    }
}
