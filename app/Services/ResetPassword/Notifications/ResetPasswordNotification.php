<?php

namespace App\Services\ResetPassword\Notifications;

use App\Services\ResetPassword\Services\ResetPasswordService;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPasswordNotification extends Notification
{
    private $type;
    private string $code;

    public function __construct($type, $code)
    {
        $this->type = $type;
        $this->code = $code;
    }

    /**
     * Get the notification's channels.
     *
     * @return array|string
     */
    public function via()
    {
        return 'mail';
//        return $this->type == ResetPasswordService::RESET_WITH_PHONE ? [SmscRuChannel::class] : 'mail';
    }

    public function toText()
    {
        $appName = config('app.name');
        $code = $this->code;
        return "Код подтверждения для восстановления пароля на сайте ${appName}: ${code}";
    }


    public function toSmscRu()
    {
//        return SmscRuMessage::create($this->toText());
    }

    /**
     * Build the mail representation of the notification.
     *
     * @return MailMessage
     */
    public function toMail()
    {
        return (new MailMessage)
            ->subject('Восстановление пароля')
            ->line($this->toText())
            ->line('Просто проигнорируйте это письмо если вы не запрашивали восстановление пароля.');
    }
}
