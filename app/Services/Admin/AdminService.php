<?php


namespace App\Services\Admin;


use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminService
{
    public function store(array $data): Admin
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'first_name' => 'required|string|max:20',
            'last_name' => 'required|string|max:20',
            'login' => 'required_without:email|string|alpha_dash|max:20|unique:admins',
            'email' => 'required_without:login|nullable|email|unique:admins',
            'password' => 'required|confirmed|min:4',
        ]);

        $admin = new Admin($data->only([
            'last_name', 'first_name', 'login', 'email',
        ])->toArray());
        $admin->password = Hash::make($data['password']);
        $admin->save();

        return $admin;
    }

    public function update(Admin $admin, array $data): Admin
    {
        $data = collect($data);

        $rules = [
            'first_name' => 'sometimes|required|string|max:30',
            'last_name' => 'sometimes|required|string|max:30',
            'email' => 'sometimes|email|required|unique:admins,email,' . $admin->id,
        ];

        if ($data->has('password') && $data['password']) {
            $rules['password'] = 'sometimes|confirmed|string|min:4';
            $rules['current_password'] = [
                'sometimes',
                'nullable',
                'required_with:password',
                function ($attribute, $value, $fail) use ($admin) {
                    if (!Hash::check($value, $admin->password)) {
                        $fail($attribute . ' is invalid.');
                    }
                }];
        }

        Validator::validate($data->toArray(), $rules);

        $admin->fill($data->only([
            'first_name', 'last_name', 'email',
        ])->toArray());

        if ($data->has('password') && $data['password']) {
            $admin->password = Hash::make($data['password']);
        }

        $admin->save();
        return $admin;
    }
}
