<?php


namespace App\Services\Attestation;


use App\Models\Attestation;
use App\Models\UserAttestationLevel;

trait HasAttestations
{
    public function attestations(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Attestation::class, 'user');
    }

    public function attestationLevels(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(UserAttestationLevel::class, 'user');
    }

    public function attestationLevel(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(UserAttestationLevel::class, 'user')
            ->orderByDesc('created_at');
    }

    public function getLevelAttribute()
    {
        return $this->attestationLevel ? $this->attestationLevel->level : null;
    }
}
