<?php


namespace App\Services\Attestation;


use App\Models\Attestation;
use App\Models\AttestationStage;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\UserAttestationLevel;
use App\Services\App\DynamicAppService;
use App\Services\App\Enums\AddictionOperations;
use App\Services\App\Enums\Formulas;
use App\Services\App\MultiplicationAppService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

const CONFIGS = [
    'mental' => [
        'addition' => [
            13 => [
                [
                    'formulas' => Formulas::None,
                    'digits_capacity' => [1, 1],
                    'task_size' => 3,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            12 => [
                [
                    'formulas' => Formulas::Five,
                    'digits_capacity' => [1, 1],
                    'task_size' => 3,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            11 => [
                [
                    'formulas' => Formulas::Ten,
                    'digits_capacity' => [1, 1],
                    'task_size' => 3,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            10 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 4,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ],
            ],
            9 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 5,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            8 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 6,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ],
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 4,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            7 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 7,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ],
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 5,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            6 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 8,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ],
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 5,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            5 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 8,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ],
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [2, 2],
                    'task_size' => 6,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            4 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [2, 2],
                    'task_size' => 8,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            3 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [2, 2],
                    'task_size' => 10,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            2 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            1 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            0 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
        ],
        'multiplication' => [
            5 => [
                [
                    'template1' => '#*',
                    'template2' => '$',
                    'is_integer' => false,
                ]
            ],
            4 => [
                [
                    'template1' => '#*',
                    'template2' => '$',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => false,
                ]
            ],
            3 => [
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => false,
                ]
            ],
            2 => [
                [
                    'template1' => '#*',
                    'template2' => '#*',
                    'is_integer' => false,
                ]
            ],
            1 => [
                [
                    'template1' => '#*',
                    'template2' => '#*',
                    'is_integer' => false,
                ],
            ],
            0 => [
                [
                    'template1' => '#*',
                    'template2' => '#**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**',
                    'template2' => '#*',
                    'is_integer' => false,
                ]
            ],
        ],
        'division' => [
            5 => [
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => true,
                ]
            ],
            4 => [
                [
                    'template1' => '#***',
                    'template2' => '$',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => true,
                ]
            ],
            3 => [
                [
                    'template1' => '#***',
                    'template2' => '$',
                    'is_integer' => true,
                ]
            ],
            2 => [
                [
                    'template1' => '#***',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '#*',
                    'is_integer' => true,
                ]
            ],
            1 => [
                [
                    'template1' => '#***',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '#*',
                    'is_integer' => true,
                ]
            ],
            0 => [
                [
                    'template1' => '#****',
                    'template2' => '#**',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#****',
                    'template2' => '#*',
                    'is_integer' => true,
                ]
            ],
        ]
    ],
    'abakus' => [
        'addition' => [
            13 => [
                [
                    'formulas' => Formulas::Five,
                    'digits_capacity' => [1, 1],
                    'task_size' => 4,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            12 => [
                [
                    'formulas' => Formulas::Ten,
                    'digits_capacity' => [1, 1],
                    'task_size' => 5,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            11 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 7,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            10 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 1],
                    'task_size' => 10,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            9 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 10,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            8 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [2, 2],
                    'task_size' => 10,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            7 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [2, 3],
                    'task_size' => 10,
                    'fraction' => 0,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            6 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 0,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            5 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [1, 2],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => AddictionOperations::Mixed,
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            4 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [2, 3],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            3 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [3, 4],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            2 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [5, 6],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            1 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [5, 6],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
            0 => [
                [
                    'formulas' => Formulas::All,
                    'digits_capacity' => [7, 8],
                    'task_size' => 10,
                    'fraction' => 2,
                    'mode' => 'digits',
                    'operations' => 'mixed',
                    'negative' => 1,
                    'operation_alt' => 0,
                    'mirror' => 0,
                    'bit_alt' => 1,
                    'without_cross_in' => 'none',
                    'bit_formulas' => 'all',
                ]
            ],
        ],
        'multiplication' => [
            8 => [
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => true,
                ]
            ],
            7 => [
                [
                    'template1' => '#*',
                    'template2' => '#*',
                    'is_integer' => true,
                ]
            ],
            6 => [
                [
                    'template1' => '#*',
                    'template2' => '#**',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
            ],
            5 => [
                [
                    'template1' => '#**',
                    'template2' => '#**',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#***',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#*',
                    'template2' => '#***',
                    'is_integer' => true,
                ],
            ],
            4 => [
                [
                    'template1' => '#***',
                    'template2' => '#**',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '#***',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#*',
                    'template2' => '#****',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#****',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
            ],
            3 => [
                [
                    'template1' => '#**.**',
                    'template2' => '#**.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*.**',
                    'template2' => '#**.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**.**',
                    'template2' => '#*.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#***',
                    'template2' => '#*.**',
                    'is_integer' => false,
                ],
            ],
            2 => [
                [
                    'template1' => '#*****.*****',
                    'template2' => '#**.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#****.*****',
                    'template2' => '#***.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*****.****',
                    'template2' => '#****.*****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**********',
                    'template2' => '#*****.***',
                    'is_integer' => false,
                ],
            ],
            1 => [
                [
                    'template1' => '#*****.*****',
                    'template2' => '#**.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#****.*****',
                    'template2' => '#***.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*****.****',
                    'template2' => '#****.*****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**********',
                    'template2' => '#*****.***',
                    'is_integer' => false,
                ],
            ],
            0 => [
                [
                    'template1' => '#*****.*****',
                    'template2' => '#**.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#****.*****',
                    'template2' => '#***.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*****.****',
                    'template2' => '#****.*****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**********',
                    'template2' => '#*****.***',
                    'is_integer' => false,
                ],
            ],
        ],
        'division' => [
            8 => [
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#*',
                    'template2' => '$',
                    'is_integer' => true,
                ]
            ],
            7 => [
                [
                    'template1' => '#***',
                    'template2' => '$',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '$',
                    'is_integer' => true,
                ]
            ],
            6 => [
                [
                    'template1' => '#***',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#**',
                    'template2' => '#*',
                    'is_integer' => true,
                ]
            ],
            5 => [
                [
                    'template1' => '#****',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#****',
                    'template2' => '#**',
                    'is_integer' => true,
                ],
            ],
            4 => [
                [
                    'template1' => '#*****',
                    'template2' => '#**',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#*****',
                    'template2' => '#***',
                    'is_integer' => true,
                ],
                [
                    'template1' => '#*****',
                    'template2' => '#*',
                    'is_integer' => true,
                ],
            ],
            3 => [
                [
                    'template1' => '#**.**',
                    'template2' => '#*.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#***.**',
                    'template2' => '#.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*.**',
                    'template2' => '#*.**',
                    'is_integer' => false,
                ],
            ],
            2 => [
                [
                    'template1' => '#****.**',
                    'template2' => '#*.****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**.**',
                    'template2' => '#*.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#***.**',
                    'template2' => '#**.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*****.**',
                    'template2' => '#**.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#***.**',
                    'template2' => '#****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#****.***',
                    'template2' => '#****',
                    'is_integer' => false,
                ],
            ],
            1 => [
                [
                    'template1' => '#****.**',
                    'template2' => '#*.****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#**.**',
                    'template2' => '#*.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#***.**',
                    'template2' => '#**.***',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#*****.**',
                    'template2' => '#**.**',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#***.**',
                    'template2' => '#****',
                    'is_integer' => false,
                ],
                [
                    'template1' => '#****.***',
                    'template2' => '#*****',
                    'is_integer' => false,
                ],
            ],
            0 => [
                [
                    'template1' => '#*********',
                    'template2' => '#*****',
                    'is_integer' => false,
                ]
            ],
        ]
    ],
];

class AttestationService
{
    public function generateVariants($data): Collection
    {
        Validator::validate($data, [
            'level' => 'required|integer|min:0|max:13',
            'count' => 'required|integer|min:1|max:30',
            'type' => 'required|in:abakus,mental'
        ]);

        $variants = collect();


        for ($i = 0; $i < $data['count']; $i++) {
            /** @var Attestation $attestation */
            $attestation = Attestation::query()->create($data);

            $this->generateStages($attestation, $data['type'], $data['level']);

            $attestation->load('stages.attestation');
            $attestation->stages->each(function ($stage) {
                $stage->makeVisible(['tasks', 'answers']);
            });

            $variants->add($attestation);
        }

        $ids = $variants->pluck('id');
        Attestation::query()->whereIn('id', $ids)->delete();
        return $variants;
    }

    function isArrayAssoc(array $arr): bool
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public function start(Model $user, array $data): Attestation
    {

        Validator::validate($data, [
            'level' => 'required|integer|min:0|max:13',
            'is_training' => 'boolean',
            'type' => 'required|in:abakus,mental'
        ]);
        $attestation = new Attestation($data);
        $attestation->user()->associate($user);
        $attestation->save();

        $this->generateStages($attestation, $data['type'], $data['level']);

        $attestation->load('stages');

        return $attestation;
    }

    public function assign(array $data): array
    {
        Validator::validate($data, [
            'student_ids' => 'required|array|min:1',
        ]);
        $users = Student::query()->whereIn('id', $data['student_ids'])->get();
        $attestations = [];
        foreach ($users as $user) {
            $attestations[] = $this->start($user, $data);
        }
        return $attestations;
    }

    private function getFirstColumnWithFiveFormulas(): array
    {
        $numbers = collect([
            collect([1, 1, 1, 1, 2]),
            collect([1, 1, 1, 2, 3]),
            collect([1, 1, 2, 2, 3]),
            collect([1, 1, 2, 2, 1]),
            collect([1, 2, 2, 2, 1]),
        ])
            ->random()
            ->shuffle();

        return [$numbers->toArray(), $numbers->sum()];
    }

    private function generateAdditionStage($attestation, $config, $type): AttestationStage
    {
        $s = app(DynamicAppService::class);
        $tasks = [];
        $answers = [];
        $cfgIdx = 0;
        for ($i = 0; $i < 10; $i++) {
            $cfg = $config[$cfgIdx];
            [$numbers, $answer] = $s->getAddendumsAndAnswer($cfg);
            $tasks[] = $numbers;
            $answers[] = $answer;
            if ($i >= (($cfgIdx + 1) * 10 / count($config)) - 1) {
                $cfgIdx++;
            }
        }

        if ($type == 'abakus') {
            // если конфиг содержит несколько вариантов формул, то используем только один вариант для таблицы самопроверки
            $config = $config[0];

            $config['task_size'] = 5;
            if ($config['formulas'] == Formulas::None) {
                $config['formulas'] = Formulas::Five;
            }

            if ($config['formulas'] === Formulas::Five) {
                [$numbers, $answer] = $this->getFirstColumnWithFiveFormulas();
            } else {
                [$numbers, $answer] = $s->getAddendumsAndAnswer(
                    array_merge($config, ['operations' => AddictionOperations::Sum])
                );
            }
            $tasks[] = $numbers;
            $answers[] = $answer;

            $rowSums = $numbers;

            for ($col = 0; $col < 3; $col++) {
                $numbers = collect();

                for ($row = 0; $row < 5; $row++) {
                    if ($row == 0) {
                        if ($config['formulas'] === Formulas::Five) {
                            $numbers->add(collect([1, 2])->random());
                        } else {
                            $config['operations'] = AddictionOperations::Sum;
                            $numbers->add($s->makeAddendum($config, 0, 0));
                        }
                    } else {
                        if ($config['formulas'] === Formulas::Five) {
                            $sign = collect([true, false])->random();
                            $prevSum = $sign
                                ? max($numbers->sum(), $rowSums[$row])
                                : min($numbers->sum(), $rowSums[$row]);
                            $variants = $s->makeVariants(Formulas::Five, $prevSum, $sign, []);
                            if ($variants->isEmpty()) {
                                $sign = !$sign;
                                $prevSum = $sign
                                    ? max($numbers->sum(), $rowSums[$row])
                                    : min($numbers->sum(), $rowSums[$row]);
                                $variants = $s->makeVariants(Formulas::Five, $prevSum, $sign, []);
                            }
                            try {
                                $number = $variants->random() * ($sign ? 1 : -1);
                            } catch (Exception $e) {
                                // на случай ошибки
                                $number = 1;
                            }
                            $numbers->add($number);
                        } else {
                            $config['operations'] = collect([AddictionOperations::Sum, AddictionOperations::Sub])->random();
                            $prevSum = $config['operations'] == AddictionOperations::Sum
                                ? max($numbers->sum(), $rowSums[$row])
                                : min($numbers->sum(), $rowSums[$row]);
                            $numbers->add($s->makeAddendum($config, $prevSum, $numbers->last()));
                        }
                    }
                }

                for ($i = 0; $i < count($rowSums); $i++) {
                    $rowSums[$i] += $numbers->get($i);
                }

                $tasks[] = $numbers->toArray();
                $answers[] = round($numbers->sum(), 2);
            }

            $answers[] = round($tasks[10][0] + $tasks[11][0] + $tasks[12][0] + $tasks[13][0], 2);
            $answers[] = round($tasks[10][1] + $tasks[11][1] + $tasks[12][1] + $tasks[13][1], 2);
            $answers[] = round($tasks[10][2] + $tasks[11][2] + $tasks[12][2] + $tasks[13][2], 2);
            $answers[] = round($tasks[10][3] + $tasks[11][3] + $tasks[12][3] + $tasks[13][3], 2);
            $answers[] = round($tasks[10][4] + $tasks[11][4] + $tasks[12][4] + $tasks[13][4], 2);
            $answers[] = round($answers[18] + $answers[17] + $answers[16] + $answers[15] + $answers[14], 2);
        }

        return new AttestationStage([
            'attestation_id' => $attestation->id,
            'tasks' => $tasks,
            'answers' => $answers,
            'type' => 'addition'
        ]);
    }

    private function generateMultiplicationStage(bool $isMultiplication, Attestation $attestation, $config): AttestationStage
    {
        $tasks = [];
        $answers = [];
        $cfgIdx = 0;
        for ($i = 0; $i < 20; $i++) {
            $cfg = $config[$cfgIdx];
            $cfg['operation'] = $isMultiplication ? 'mul' : 'div';
            [$numbers, $answer] = app(MultiplicationAppService::class)->getNumbersAndAnswer($cfg);
            $tasks[] = $numbers;
            $answers[] = $answer;
            if ($i >= (($cfgIdx + 1) * 20 / count($config)) - 1) {
                $cfgIdx++;
            }
        }

        return new AttestationStage([
            'attestation_id' => $attestation->id,
            'tasks' => $tasks,
            'answers' => $answers,
            'type' => $isMultiplication ? 'multiplication' : 'division'
        ]);
    }

    private function calcStageScore(AttestationStage $stage): int
    {
        $elapsedSeconds = Carbon::now()->diffInSeconds($stage->started_at);
        $totalTime = $stage->attestation->type == 'mental' ? 180 : 600;
        if ($elapsedSeconds > $totalTime + 10) {
            return 0;
        }

        $res = 0;
        $userAnswers = collect($stage->user_answers);
        $trueAnswers = collect($stage->answers);
        if ($stage->type == 'addition') {
            $trueAnswers->each(function ($item, $idx) use ($userAnswers, &$res, $stage) {
                if ((string)$userAnswers->get($idx) === (string)$item) {
                    $res += $idx > 9 ? 5 : 10;
                }
            });
        } else {
            $trueAnswers->each(function ($item, $idx) use ($userAnswers, &$res, $stage) {
                if ((string)$userAnswers->get($idx) === (string)$item) {
                    $res += 5;
                }
            });
        }
        return $res;
    }

    public function checkAttestationEnd(int $attestationId)
    {
        /* @var $attestation Attestation */
        $attestation = Attestation::query()
            ->with(['stages'])
            ->withSum('stages', 'score')
            ->findOrFail($attestationId);
        if ($attestation->stages->every('finished_at', '!=', null)) {
            $attestation->finished_at = Carbon::now();
            $attestation->save();
        }

        if (!$attestation->is_training && $attestation->finished_at && $attestation->score_percent >= 0.7) {
            /** @var Student|Teacher $user */
            $user = Auth::user();
            if (!$user->attestationLevel || ($user->attestationLevel->level > $attestation->level)) {
                $this->grantNewAttestationLevel($user, $attestation->level, $attestation->id, Carbon::now());
            }
        }
    }

    public function grantNewAttestationLevel($user, $level, $attestationId, Carbon $date): \Illuminate\Database\Eloquent\Model
    {
        if ($user instanceof Student) {
            $user->load('group.teacher');
            if ($user->group) {
                $teacher_id = $user->group->teacher->id;
            }
        }

        /** @var UserAttestationLevel $attLevel */
        $attLevel = $user->attestationLevel()->create([
            'level' => $level,
            'attestation_id' => $attestationId,
            'issued_teacher_id' => $teacher_id ?? null
        ]);


        try {
            Artisan::call('make:certificate', [
                '--fio' => $user->full_name,
                '--level' => $level,
                '--date' => $date->isoFormat('DD.MM.YYYY')
            ]);
            $path = Artisan::output();
            $path = substr($path, 0, -1); // remove \n


            $attLevel->certificate = $attLevel->certificate()->create([
                'name' => 'Сертификат аттестации.png',
                'disk' => 'public',
                'original' => $path,
                'type' => 'image/png'
            ]);
        } catch (Exception $e) {
        }
        return $attLevel;
    }

    public function getStage($stageId, $start = false): AttestationStage
    {
        /** @var AttestationStage $stage */
        $stage = AttestationStage::query()
            ->with('attestation')
            ->findOrFail($stageId);

        if (!$stage->started_at && $start) {
            if (!$stage->attestation->user->has_access) {
                abort(401, 'Для прохождения аттестаций необходима подписка.');
            }

            $stage->started_at = Carbon::now();
            $stage->save();
        }
        if ($stage->started_at) {
            $stage->makeVisible('tasks');
        }
        if ($stage->finished_at) {
            $stage->makeVisible('answers');
        }
        return $stage;
    }

    public function answerStage($stageId, array $userAnswers): AttestationStage
    {
        /** @var AttestationStage $stage */
        $stage = AttestationStage::query()->with('attestation')->findOrFail($stageId);
        if ($stage->finished_at) {
            throw new BadRequestHttpException('Этап уже завершен.');
        }

        $stage->user_answers = $userAnswers;
        $stage->score = $this->calcStageScore($stage);
        $stage->finished_at = Carbon::now();
        $stage->save();

        $this->checkAttestationEnd($stage->attestation_id);

        $stage->makeVisible(['tasks', 'answers']);
        return $stage;
    }

    /**
     * @param Attestation $attestation
     * @param $type
     * @param $level
     * @return void
     */
    public function generateStages(Attestation $attestation, $type, $level): void
    {
        $configs = CONFIGS[$type];

        $addictionStage = $this->generateAdditionStage($attestation, $configs['addition'][$level], $type);
        $addictionStage->save();

        if (array_key_exists($level, $configs['multiplication'])) {
            $multiplicationStage = $this->generateMultiplicationStage(
                true, $attestation, $configs['multiplication'][$level]);
            $multiplicationStage->save();
        }

        if (array_key_exists($level, $configs['division'])) {
            $divisionStage = $this->generateMultiplicationStage(
                false, $attestation, $configs['division'][$level]);
            $divisionStage->save();
        }
    }
}
