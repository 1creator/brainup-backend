<?php


namespace App\Services\Subscription;


use App\Models\Subscription;
use App\Models\SubscriptionTransaction;

/**
 * @property mixed|null lastSubscription
 * @property mixed subscriptions
 */
trait HasSubscriptions
{
    public function subscriptionTransactions()
    {
        return $this->morphMany(SubscriptionTransaction::class, 'owner')->orderByDesc('created_at');
    }

    public function subscriptions()
    {
        return $this->morphMany(Subscription::class, 'user')->orderByDesc('expired_at');
    }

    public function lastSubscription()
    {
        return $this->morphOne(Subscription::class, 'user')->orderByDesc('expired_at');
    }

    public function getHasAccessAttribute()
    {
        if ($this->lastSubscription && !$this->lastSubscription->isExpired) {
            return $this->lastSubscription->expired_at;
        } else {
            return false;
        }
    }
}
