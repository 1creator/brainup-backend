<?php


namespace App\Services\Subscription\Notifications;


use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TeacherTestSubscriptionNotification extends Notification
{

    /**
     * @var null
     */
    private $password;

    public function __construct($password = null)
    {
        $this->password = $password;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $msg = (new MailMessage)
            ->subject('Пробный период Brain-up.ru')
            ->greeting('Привет!')
            ->line("Мы рады что вы зарегистрировались на нашей платформе в качестве преподавателя!")
            ->line("В течение следующего месяца вы можете бесплатно использовать все возможности платформы.")
            ->line("Помимо этого, мы дарим вам 5 месячных абонементов для ваших учеников. "
                . "Вы сможете передать абонемент ученику сразу после того, как добавите его к одной из своих групп.");
        if ($this->password) {
            $msg->line("Пароль для авторизации: " . $this->password);
        }
        $msg->action('В личный кабинет', config('services.frontend_url') . '/teacher/subscriptions');
        return $msg;
    }
}
