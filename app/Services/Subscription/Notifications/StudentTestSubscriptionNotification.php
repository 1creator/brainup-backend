<?php


namespace App\Services\Subscription\Notifications;


use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StudentTestSubscriptionNotification extends Notification
{

    /**
     * @var null
     */
    private $password;

    public function __construct($password = null)
    {
        $this->password = $password;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $msg = (new MailMessage)
            ->subject('Зарегистрирован аккаунт на сайта Brain-up.ru')
            ->greeting('Привет!')
            ->line("Мы рады что ты зарегистрировался на нашей платформе в качестве ученика!")
            ->line("В течение следующего месяца ты можешь бесплатно использовать все возможности платформы.");
        if ($this->password) {
            $msg->line("Пароль для авторизации: " . $this->password);
        }
        $msg->action('В личный кабинет', config('services.frontend_url') . '/student/subscriptions');
        return $msg;
    }
}
