<?php


namespace App\Services\Subscription;


use Illuminate\Database\Eloquent\Model;

interface HasSubscription
{
    public function subscriptions();

    public function lastSubscription();

    public function subscriptionTransactions();

    public function getHasAccessAttribute();
}
