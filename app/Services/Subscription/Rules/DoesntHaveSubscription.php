<?php


namespace App\Services\Subscription\Rules;


use App\Models\Student;
use App\Services\Subscription\HasSubscription;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class DoesntHaveSubscription implements Rule
{
    private string $class;

    public function __construct($class = Student::class)
    {
        $this->class = $class;
    }

    public function passes($attribute, $value)
    {
        /** @var HasSubscription $student */
        $model = call_user_func([$this->class, 'findOrFail'], $value);
        return !$model->has_access;
    }

    public function message()
    {
        return 'Пользователь уже имеет активную подписку.';
    }
}
