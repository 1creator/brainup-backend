<?php


namespace App\Services\Subscription;


use App\Models\Student;
use App\Models\Subscription;
use App\Models\SubscriptionTransaction;
use App\Models\Teacher;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Models\Payment;
use App\Services\Variables\VariablesService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SubscriptionService
{
    const TEACHER_PACKAGE = 'TEACHER_PACKAGE';
    const TEACHER_SUBSCRIPTION = 'TEACHER_SUBSCRIPTION';
    const STUDENT_SUBSCRIPTION = 'STUDENT_SUBSCRIPTION';

    public function store(HasSubscription $model, array $data, $description = 'Получен абонемент'): Subscription
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'expired_at' => [
                function ($attribute, $value, $fail) use ($model) {
                    if ($model->lastSubscription && !$model->lastSubscription->is_expired) {
                        $fail('Пользователь уже имеет активный абонемент');
                    }
                },
                'nullable',
                'date',
            ],
        ]);

        $subscription = new Subscription($data->only(['expired_at'])->toArray());

        /** @var Model $model */
        $subscription->user()->associate($model);
        $subscription->save();

        $subscriptionTransaction = new SubscriptionTransaction([
            'description' => $description,
        ]);
        $model->subscriptionTransactions()->save($subscriptionTransaction);

        return $subscription;
    }

    public function update(Subscription $subscription, array $data): Subscription
    {
        $data = collect($data);
        $rules = [
            'expired_at' => 'required|date',
        ];
        Validator::validate($data->toArray(), $rules);

        $subscription->fill($data->only(['expired_at',])->toArray());
        $subscription->save();

        return $subscription;
    }

    public function giveSubscription(Teacher $teacher, Student $student)
    {
        $transaction1 = new SubscriptionTransaction([
            'owner_type' => Teacher::class,
            'owner_id' => $teacher->id,
            'description' => 'Передача абонемента ученику: ' . $student->short_name,
        ]);

        $transaction2 = new SubscriptionTransaction([
            'owner_type' => Student::class,
            'owner_id' => $student->id,
            'description' => 'Приём абонемента от преподавателя: ' . $teacher->short_name,
        ]);

        $subscription = new Subscription([
            'user_type' => Student::class,
            'user_id' => $student->id,
            'expired_at' => Carbon::today()->addMonth(),
        ]);

        $transaction1->save();
        $transaction2->save();
        $subscription->save();

        $teacher->subscriptions_left--;
        $teacher->save();

        return $subscription;
    }

    public function getRedirectForPackageBuy(array $data): string
    {
        $variables = app(VariablesService::class)->getAll([
            'subscriptionPackagePrice5',
            'subscriptionPackagePrice30',
            'subscriptionPackagePrice50',
        ]);

        $prices = [
            '5' => $variables['subscriptionPackagePrice5'],
            '30' => $variables['subscriptionPackagePrice30'],
            '50' => $variables['subscriptionPackagePrice50'],
        ];
        $amount = $prices[$data['count']];
        $returnUrl = url('/payment/success');
        $description = 'Покупка пакета абонементов';
        $info = [
            'type' => self::TEACHER_PACKAGE,
            'count' => (int)$data['count'],
            'teacher_id' => Auth::id(),
        ];

        return app(PaymentInterface::class)
            ->makeRedirect($amount, $description, $returnUrl, null, $info);
    }

    public function getRedirectForTeacherSubscriptionBuy(array $data): string
    {
        $variables = app(VariablesService::class)->getAll([
            'subscriptionTeacherPrice1',
            'subscriptionTeacherPrice6',
            'subscriptionTeacherPrice12',
        ]);
        $prices = [
            '1' => $variables['subscriptionTeacherPrice1'],
            '6' => $variables['subscriptionTeacherPrice6'],
            '12' => $variables['subscriptionTeacherPrice12'],
        ];
        $amount = $prices[$data['count']];
        $returnUrl = url('/payment/success');
        $description = 'Приобретена подписка на доступ в личный кабинет преподавателя: ' . $data['count'] . ' мес.';
        $info = [
            'type' => self::TEACHER_SUBSCRIPTION,
            'count' => (int)$data['count'],
            'teacher_id' => Auth::id(),
        ];

        return app(PaymentInterface::class)
            ->makeRedirect($amount, $description, $returnUrl, null, $info);
    }

    public function getRedirectForStudentSubscriptionBuy(array $data): string
    {
        $variables = app(VariablesService::class)->getAll([
            'subscriptionStudentPrice1',
            'subscriptionStudentPrice6',
            'subscriptionStudentPrice12',
        ]);
        $prices = [
            '1' => $variables['subscriptionStudentPrice1'],
            '6' => $variables['subscriptionStudentPrice6'],
            '12' => $variables['subscriptionStudentPrice12'],
        ];
        $amount = $prices[$data['count']];
        $returnUrl = url('/payment/success');
        $description = 'Приобретен абонемент: ' . $data['count'] . ' мес.';
        $info = [
            'type' => self::STUDENT_SUBSCRIPTION,
            'count' => (int)$data['count'],
            'student_id' => Auth::id(),
        ];

        return app(PaymentInterface::class)
            ->makeRedirect($amount, $description, $returnUrl, null, $info);
    }

    public function resolvePackagePayment(Payment $payment)
    {
        /** @var Teacher $teacher */
        $teacher = Teacher::findOrFail($payment['info']['teacher_id']);
        $tr = new SubscriptionTransaction([
            'description' => 'Приобретён пакет абонементов: ' . $payment['info']['count'] . ' шт.'
        ]);
        $teacher->subscriptionTransactions()->save($tr);
        $teacher->subscriptions_left += $payment['info']['count'];
        $teacher->save();

        return 'ok';
    }

    public function resolveTeacherSubscriptionPayment(Payment $payment)
    {
        /** @var Teacher $teacher */
        $teacher = Teacher::findOrFail($payment['info']['teacher_id']);

        /** @var Subscription $lastSubscription */
        $lastSubscription = $teacher->lastSubscription;

        $expired_at = $lastSubscription && $lastSubscription->is_expired
            ? Carbon::now()->addMonths($payment['info']['count'])
            : $lastSubscription->expired_at->addMonths($payment['info']['count']);

        $subscription = new Subscription([
            'expired_at' => $expired_at,
        ]);
        $teacher->subscriptions()->save($subscription);

        $tr = new SubscriptionTransaction([
            'description' => 'Приобретена подписка на доступ в личный кабинет преподавателя.'
        ]);
        $teacher->subscriptionTransactions()->save($tr);

        return 'ok';
    }

    public function resolveStudentSubscriptionPayment(Payment $payment)
    {
        /** @var Student $student */
        $student = Student::findOrFail($payment['info']['student_id']);

        /** @var Subscription $lastSubscription */
        $lastSubscription = $student->lastSubscription;

        if ($lastSubscription && !$lastSubscription->is_expired) {
            $expired_at = $lastSubscription->expired_at->addMonths($payment['info']['count']);
        } else {
            $expired_at = Carbon::now()->addMonths($payment['info']['count']);
        }

        $subscription = new Subscription([
            'expired_at' => $expired_at,
        ]);
        $student->subscriptions()->save($subscription);

        $tr = new SubscriptionTransaction([
            'description' => 'Приобретен абонемент: ' . $payment['info']['count'] . ' мес.'
        ]);
        $student->subscriptionTransactions()->save($tr);

        return 'ok';
    }
}
