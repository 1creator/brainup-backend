<?php


namespace App\Services\Variables;


use App\Models\Variable;

class VariablesService
{
    public function set(string $id, string $value)
    {
        Variable::query()->updateOrCreate(['id' => $id], ['value' => $value]);
        return Variable::query()->find($id);
    }

    public function update(array $items): \Illuminate\Database\Eloquent\Collection
    {
        $ids = [];
        foreach ($items as $item) {
            Variable::query()->updateOrCreate(['id' => $item['id']], [
                'value' => json_encode($item['value'])
            ]);
            $ids[] = $item['id'];
        }
        return Variable::query()->findMany($ids);
    }

    public function get(string $id)
    {
        $res = Variable::query()->find($id);
        return $res ? json_decode($res->value) : null;
    }

    public function getAll(?array $ids = null)
    {
        $query = Variable::query();
        if ($ids) {
            $query = $query->findMany($ids);
        } else {
            $query = $query->get();
        }
        return $query->mapWithKeys(fn($item, $key) => [$item['id'] => json_decode($item['value'])]);
    }
}
