<?php


namespace App\Http\Controllers;


use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Models\Payment;
use Illuminate\Http\Request;

class PaymentController
{
    /** @noinspection PhpVoidFunctionResultUsedInspection */
    public function success(Request $request)
    {

        app(PaymentInterface::class)->resolveWebHook($request);


        return abort(404, 'Что-то пошло не так. Пожалуйста, напишите нам report@brain-up.ru и мы всё исправим.');
    }

    public function getAccountType(Request $request, $payment)
    {
        $payment = Payment::with('owner')->findOrFail($payment);
        return $payment->owner->account_type;
    }

    /** @noinspection PhpVoidFunctionResultUsedInspection */
    public function resolveNotification(Request $request)
    {
        app(PaymentInterface::class)->resolveWebHook($request);
    }
}
