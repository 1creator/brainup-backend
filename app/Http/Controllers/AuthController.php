<?php


namespace App\Http\Controllers;


use App\Models\Admin;
use App\Models\Student;
use App\Models\Teacher;
use App\Services\ResetPassword\Services\ResetPasswordService;
use App\Services\Student\StudentService;
use App\Services\Teacher\TeacherService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController
{
    public function login(Request $request)
    {
        switch ($request->get('type')) {
            case 'admin':
                $user = Admin::query()
                    ->where('email', $request->get('username'))
                    ->first();

                if ($user && Hash::check($request->get('password'), $user['password'])) {
                    return [
                        'user' => $user,
                        'api_token' => $user->api_token
                    ];
                }
                break;
            case 'teacher':
                $user = Teacher::query()
                    ->where('email', $request->get('username'))
                    ->first();

                if ($user && Hash::check($request->get('password'), $user['password'])) {
                    $user->append('has_access');
                    $user->append(['has_access', 'level']);
                    return [
                        'user' => $user,
                        'api_token' => $user->api_token
                    ];
                }
                break;
            case 'student':
            default:
                $user = Student::query()
                    ->where('login', $request['username'])
                    ->orWhere('email', $request['username'])
                    ->first();

                if ($user && Hash::check($request->get('password'), $user['password'])) {
                    $user->append(['brains', 'has_access']);
                    $user->append(['has_access', 'level']);
                    $user->load('visualPreferences');

                    return [
                        'user' => $user,
                        'api_token' => $user->api_token
                    ];
                }
        }

        abort(401, "Неправильный логин или пароль");
    }

    public function register(Request $request)
    {
        switch ($request->get('type')) {
            case 'teacher':
                $service = app(TeacherService::class);
                $user = $service->store($request->all());
                $service->giveTestSubscription($user, $request->input('password'));
                $user->append('has_access');
                break;
            case 'student':
            default:
                $service = app(StudentService::class);
                $user = $service->store($request->all());
                $service->giveTestSubscription($user, $request->input('password'));
                $user->append(['brains', 'has_access']);
                $user->load('visualPreferences');
                break;
        }

        return [
            'user' => $user,
            'api_token' => $user->api_token
        ];
    }

    public function resetPassword(Request $request)
    {
        switch ($request->get('type')) {
            case 'teacher':
                return app(ResetPasswordService::class)->send($request->toArray(), Teacher::class);
            case 'student':
            default:
                return app(ResetPasswordService::class)->send($request->toArray(), Student::class);
        }
    }

    public function resetPasswordConfirm(Request $request)
    {
        return app(ResetPasswordService::class)->reset($request->toArray());
    }
}
