<?php

namespace App\Http\Controllers\Admin;

use App\Models\Teacher;
use App\Services\Teacher\TeacherService;
use App\Utils\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use Spatie\QueryBuilder\AllowedFilter;

class TeacherController extends Controller
{
    public function index()
    {
        return QueryBuilder::for(Teacher::class)
            ->allowedIncludes(['groups', 'students', 'lastSubscription'])
            ->allowedFilters(AllowedFilter::callback('query', function (Builder $query, $val) {
                $query->where('last_name', 'like', '%' . $val . '%')
                    ->orWhere('email', 'like', '%' . $val . '%')
                    ->orWhere('phone', 'like', '%' . $val . '%');
            }, null, '|'))
            ->with(['lastSubscription', 'attestationLevel'])
            ->get()
            ->append(['level']);
    }

    public function store(Request $request)
    {
        return app(TeacherService::class)->store($request->all());
    }

    public function update(Request $request, $teacher)
    {
        $teacher = Teacher::findOrFail($teacher);
        return app(TeacherService::class)->update($teacher, $request->all());
    }

    public function show(Request $request, $teacher)
    {
        return QueryBuilder::for(Teacher::class)
            ->allowedIncludes(['groups.students', 'students.lastSubscription', 'subscriptions', 'apps'])
            ->with(['lastSubscription', 'attestationLevel'])
            ->findOrFail($teacher)
            ->append(['level']);
    }

    public function destroy(Request $request, $teacher)
    {
        return Teacher::destroy($teacher) ? 1 : 0;
    }
}
