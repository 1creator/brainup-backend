<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attestation;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class AttestationController extends Controller
{
    public function index()
    {
        return QueryBuilder::for(Attestation::class)
            ->withSum('stages', 'score')
            ->with(['user'])
            ->defaultSort('-created_at')
            ->get();
    }

    public function show(Request $request, $attestation): Attestation
    {
        /** @var Attestation $res */
        $res = Attestation::query()
            ->withSum('stages', 'score')
            ->with(['user', 'stages', 'stages.attestation', 'attestationLevelDetails'])
            ->findOrFail($attestation);
        $res->stages->each(fn($stage) => $stage->makeVisible(['tasks', 'answers']));
        return $res;
    }
}
