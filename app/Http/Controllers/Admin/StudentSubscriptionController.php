<?php

namespace App\Http\Controllers\Admin;

use App\Models\Student;
use App\Models\Subscription;
use App\Services\Subscription\SubscriptionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class StudentSubscriptionController extends Controller
{
    public function index($student)
    {
        return QueryBuilder::for(Subscription::class)
            ->where('user_id', $student)
            ->where('user_type', Student::class)
            ->get();
    }

    public function store(Request $request, $student)
    {
        $student = Student::findOrFail($student);
        return app(SubscriptionService::class)->store($student, $request->all());
    }
}
