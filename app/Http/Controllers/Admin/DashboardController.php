<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        /** @var Admin $user */
        $user = Auth::user();
        return [
            'user' => $user,
        ];
    }
}
