<?php

namespace App\Http\Controllers\Admin;

use App\Models\Student;
use App\Models\Subscription;
use App\Services\Subscription\SubscriptionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class SubscriptionController extends Controller
{
    public function destroy(Request $request, $subscription)
    {
        return Subscription::destroy($subscription);
    }
}
