<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseApp;
use App\Models\Student;
use App\Services\App\AppService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;

class FreeAppController extends Controller
{
    public function index(Request $request)
    {
        return BaseApp::query()->join('free_apps', 'apps.id', '=', 'free_apps.app_id')->get();
    }

    public function store(Request $request)
    {
        $app = app(AppService::class)->store($request->all());
        DB::table('free_apps')->insert(['app_id' => $app->id]);
        $data = Student::select('id')->get()->map(
            function ($item) use ($app) {
                return [
                    'student_id' => $item['id'],
                    'app_id' => $app['id'],
                ];
            })->toArray();
        DB::table('student_app')->insert($data);
        return $app;
    }

    public function update(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppService::class)->update($app, $request->all());
    }

    public function show(Request $request, $app)
    {
        return QueryBuilder::for(BaseApp::class)
            ->allowedIncludes(['students'])
            ->findOrFail($app);
    }

    public function destroy(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppService::class)->destroy($app);
    }
}
