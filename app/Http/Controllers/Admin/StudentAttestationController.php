<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attestation;
use App\Models\Student;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class StudentAttestationController extends Controller
{
    public function index(Request $request, $student)
    {
        return QueryBuilder::for(Attestation::class)
            ->where('user_type', Student::class)
            ->where('user_id', $student)
            ->with(['stages', 'attestationLevelDetails'])
            ->withSum('stages', 'score')
            ->get();
    }
}
