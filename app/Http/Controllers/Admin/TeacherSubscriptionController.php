<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subscription;
use App\Models\Teacher;
use App\Services\Subscription\SubscriptionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class TeacherSubscriptionController extends Controller
{
    public function index($teacher)
    {
        return QueryBuilder::for(Subscription::class)
            ->where('user_id', $teacher)
            ->where('user_type', Teacher::class)
            ->get();
    }

    public function store(Request $request, $teacher)
    {
        $teacher = Teacher::findOrFail($teacher);
        return app(SubscriptionService::class)->store($teacher, $request->all());
    }
}
