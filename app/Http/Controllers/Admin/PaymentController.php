<?php

namespace App\Http\Controllers\Admin;

use App\Services\Payment\Models\Payment;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class PaymentController extends Controller
{
    public function index()
    {
        return QueryBuilder::for(Payment::class)
            ->allowedIncludes(['owner'])
            ->with('owner')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function show(Request $request, $payment)
    {
        return QueryBuilder::for(Payment::class)
            ->allowedIncludes(['owner'])
            ->findOrFail($payment);
    }

    public function destroy(Request $request, $payment)
    {
        return Payment::destroy($payment) ? 1 : 0;
    }
}
