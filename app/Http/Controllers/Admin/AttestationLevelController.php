<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserAttestationLevel;
use Laravel\Lumen\Routing\Controller;

class AttestationLevelController extends Controller
{
    public function destroy($id)
    {
        $level = UserAttestationLevel::query()->with('user')->findOrFail($id);
        $level->delete();
        return $level->user->attestationLevel;
    }
}
