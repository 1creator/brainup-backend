<?php

namespace App\Http\Controllers\Admin;

use App\Services\Variables\VariablesService;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class VariablesController extends Controller
{
    public function index()
    {
        return app(VariablesService::class)->getAll();
    }

    public function update(Request $request)
    {
        return app(VariablesService::class)->update($request->get('items'));
    }
}
