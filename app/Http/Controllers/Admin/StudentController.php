<?php

namespace App\Http\Controllers\Admin;

use App\Models\Student;
use App\Services\Student\StudentService;
use App\Utils\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use Spatie\QueryBuilder\AllowedFilter;

class StudentController extends Controller
{
    public function index()
    {
        return QueryBuilder::for(Student::class)
            ->allowedIncludes(['lastSubscription', 'lastBrains'])
            ->allowedAppends(['brains'])
            ->allowedFilters(AllowedFilter::callback('query', function (Builder $query, $val) {
                $query->where('last_name', 'like', '%' . $val . '%')
                    ->orWhere('email', 'like', '%' . $val . '%')
                    ->orWhere('phone', 'like', '%' . $val . '%')
                    ->orWhere('login', 'like', '%' . $val . '%');
            }, null, '|'))
            ->with(['lastSubscription', 'attestationLevel', 'lastBrains'])
            ->get()
            ->append(['average_quality', 'level', 'brains']);
    }

    public function store(Request $request)
    {
        return app(StudentService::class)->store($request->all());
    }

    public function update(Request $request, $student)
    {
        $student = Student::findOrFail($student);
        return app(StudentService::class)->update($student, $request->all());
    }

    public function show(Request $request, $student)
    {
        return QueryBuilder::for(Student::class)
            ->allowedIncludes(['finishedSessions', 'apps', 'brainsHistory', 'lastSubscription', 'subscriptions', 'group.teacher.groups'])
            ->allowedAppends(['averageQuality', 'appsWithStats', 'brains', 'attestationLevel'])
            ->findOrFail($student)
            ->append(['level']);
    }

    public function destroy(Request $request, $student)
    {
        return Student::destroy($student) ? 1 : 0;
    }
}
