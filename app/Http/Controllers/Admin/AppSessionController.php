<?php

namespace App\Http\Controllers\Admin;

use App\Models\AppSession;
use App\Models\BaseApp;
use App\Models\Student;
use App\Models\Teacher;
use App\Services\App\AppSessionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class AppSessionController extends Controller
{
    public function play(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppSessionService::class)->play($app, $request->all());
    }

    public function show(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        $appSession->append('app_type');
        return $appSession;
    }

    public function restart(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->restart($appSession, $request->all());
    }

    public function updateSettings(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->updateSettings($appSession, $request->all());
    }

    public function storeAnswer(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->answer($appSession, $request->all());
    }

    public function getTask(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->getTask($appSession);
    }
}
