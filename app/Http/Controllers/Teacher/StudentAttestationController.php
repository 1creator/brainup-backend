<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Attestation;
use App\Models\Student;
use App\Models\Teacher;
use App\Services\Attestation\AttestationService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;
use Laravel\Lumen\Routing\Controller;

class StudentAttestationController extends Controller
{
    public function index(Request $request, $student)
    {
        return QueryBuilder::for(Attestation::class)
            ->where('user_type', Student::class)
            ->where('user_id', $student)
            ->with(['stages', 'attestationLevelDetails'])
            ->withSum('stages', 'score')
            ->defaultSort('-created_at')
            ->get();
    }

    public function grantLevel(Request $request, $student)
    {

        Validator::validate($request->all(), [
            'level' => 'required|integer|min:1|max:13',
        ]);

        /** @var Teacher $teacher */
        $teacher = Auth::user();
        $student = $teacher->students()->findOrFail($student);
        return app(AttestationService::class)
            ->grantNewAttestationLevel($student, $request->input('level'), null, Date::now());
    }
}
