<?php

namespace App\Http\Controllers\Teacher;

use App\Models\BrainValue;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class StudentBrainsController extends Controller
{
    public function index(Request $request, $student)
    {
        return QueryBuilder::for(BrainValue::class)
            ->where('student_id', $student)
            ->get();
    }
}
