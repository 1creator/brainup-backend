<?php

namespace App\Http\Controllers\Teacher;

use App\Models\BaseApp;
use App\Models\Student;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class StudentAppController extends Controller
{
    public function index(Request $request, $student)
    {
        $apps = QueryBuilder::for(BaseApp::class)
            ->defaultSort('-created_at')
            ->allowedFilters(['is_homework'])
            ->whereHas('students', function ($q) use ($student) {
                $q->where('students.id', $student);
            })
            ->with([
                'bestSession' => function ($q) use ($student) {
                    $q->where('player_id', $student)->where('player_type', Student::class);
                },
                'lastSession' => function ($q) use ($student) {
                    $q->where('player_id', $student)->where('player_type', Student::class);
                },
            ])
            ->get();
        $apps->each->append('is_owned');
//        $apps->each->append('userMaxQuality');
        return $apps;
    }
}
