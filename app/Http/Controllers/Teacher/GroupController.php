<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Group;
use App\Services\Group\GroupService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class GroupController extends Controller
{
    public function index()
    {
        return QueryBuilder::for(Group::class)
            ->where('teacher_id', '=', Auth::id())
            ->with('schedules')
            ->get();
    }

    public function store(Request $request)
    {
        $group = app(GroupService::class)->store($request->all());
        $group->load('students');
        return $group;
    }

    public function update(Request $request, $group)
    {
        $group = Group::findOrFail($group);
        $group = app(GroupService::class)->update($group, $request->all());
        $group->load('students');
        return $group;
    }

    public function destroy(Request $request, $group)
    {
        $group = Group::findOrFail($group);
        return app(GroupService::class)->destroy($group);
    }

    public function show(Request $request, $group)
    {
        return QueryBuilder::for(Group::class)->findOrFail($group);
    }
}
