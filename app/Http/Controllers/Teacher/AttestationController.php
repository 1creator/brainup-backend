<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Attestation;
use App\Models\Student;
use App\Models\Teacher;
use App\Services\Attestation\AttestationService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class AttestationController extends Controller
{
    public function index()
    {
        /** @var Teacher|Student $user */
        $user = Auth::user();
        return QueryBuilder::for($user->attestations())
            ->withSum('stages', 'score')
            ->defaultSort('-created_at')
            ->get();
    }

    public function store(Request $request)
    {
        /** @var Teacher $user */
        $user = Auth::user();
        return app(AttestationService::class)->start($user, $request->all());
    }

    public function assign(Request $request)
    {
        return app(AttestationService::class)->assign($request->all());
    }

    public function generateVariants(Request $request)
    {
        return app(AttestationService::class)
            ->generateVariants($request->all());
    }

    public function show(Request $request, $attestation)
    {
        /** @var Teacher|Student $user */
        $user = Auth::user();

        /** @var Attestation $attestation */
        return $user->attestations()
            ->with(['stages', 'attestationLevelDetails'])
            ->withSum('stages', 'score')
            ->findOrFail($attestation);
    }

    public function destroy(Request $request, $attestation)
    {
        /** @var Teacher|Student $user */
        $user = Auth::user();

        /** @var Attestation $attestation */
        $attestation = $user->attestations()->with('stages')->findOrFail($attestation);
        if (!$attestation->is_training) {
            throw ValidationException::withMessages(['is_training' => 'Нельзя удалить нетренировочную аттестацию.']);
        }

        $attestation->delete();

        return $attestation;
    }
}
