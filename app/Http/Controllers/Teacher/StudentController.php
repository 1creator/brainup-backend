<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Student;
use App\Models\Teacher;
use App\Services\Student\StudentService;
use App\Utils\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Concerns\ValidatesAttributes;
use Laravel\Lumen\Routing\Controller;

class StudentController extends Controller
{
    use ValidatesAttributes;

    public function index()
    {
        return QueryBuilder::for(Student::class)
            ->whereHas('group', function (Builder $q) {
                $q->where('teacher_id', '=', Auth::id());
            })
            ->with(['lastSubscription', 'attestationLevel', 'lastBrains'])
            ->get()
            ->append(['average_quality', 'level', 'brains']);
    }

    public function store(Request $request)
    {
        return app(StudentService::class)->store($request->all());
    }

    public function invite(Request $request)
    {
        Validator::validate($request->toArray(), [
            'email' => 'exists:students,email'
        ]);

        $student = Student::where('email', $request['email'])->firstOrFail();

        /** @var Teacher $user */
        $user = Auth::user();
        return app(StudentService::class)->invite($user, $student);
    }

    public function update(Request $request, Student $student)
    {
        return app(StudentService::class)->update($student, $request->all());
    }

    public function destroy(Request $request, $student)
    {
        $student = Student::findOrFail($student);
        $student->group()->dissociate()->save();
        return 1;
    }

    public function show(Request $request, $student)
    {
        return QueryBuilder::for(Student::class)->findOrFail($student);
    }
}
