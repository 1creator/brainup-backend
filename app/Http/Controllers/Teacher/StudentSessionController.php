<?php

namespace App\Http\Controllers\Teacher;

use App\Models\AppSession;
use App\Models\Student;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class StudentSessionController extends Controller
{
    public function index(Request $request, $student)
    {
        //        $apps->each->append('is_owned');
//        $apps->each->append('userMaxQuality');
        return QueryBuilder::for(AppSession::class)
            ->with('app')
            ->defaultSort('-updated_at')
            ->where('player_type', Student::class)
            ->where('player_id', $student)
            ->get();
    }
}
