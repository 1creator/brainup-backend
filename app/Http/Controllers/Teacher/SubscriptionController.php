<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Student;
use App\Models\Teacher;
use App\Services\Student\Rules\StudentBelongsToTeacher;
use App\Services\Subscription\Rules\DoesntHaveSubscription;
use App\Services\Subscription\SubscriptionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Lumen\Routing\Controller;

class SubscriptionController extends Controller
{
    public function give(Request $request)
    {
        Validator::validate($request->all(), [
            'student_id' => [
                'required', new StudentBelongsToTeacher(), new DoesntHaveSubscription(Student::class)
            ]
        ]);

        /** @var Teacher $teacher */
        $teacher = Auth::user();

        if ($teacher->subscriptions_left <= 0) {
            abort(422, 'У вас отсутствуют абонементы, которые вы можете передавать ученикам.');
        }

        /** @var Student $student */
        $student = Student::findOrFail($request->get('student_id'));

        return app(SubscriptionService::class)->giveSubscription($teacher, $student);
    }

    public function transactions(Request $request)
    {
        /** @var Teacher $teacher */
        $teacher = Auth::user();
        return QueryBuilder::for($teacher->subscriptionTransactions()->getQuery())->get();
    }

    public function buyPackage(Request $request)
    {
        return app(SubscriptionService::class)->getRedirectForPackageBuy($request->all());
    }

    public function buySubscription(Request $request)
    {
        return app(SubscriptionService::class)->getRedirectForTeacherSubscriptionBuy($request->all());
    }
}
