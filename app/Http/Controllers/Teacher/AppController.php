<?php

namespace App\Http\Controllers\Teacher;

use App\Models\BaseApp;
use App\Services\App\AppService;
use App\Services\App\AppSessionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class AppController extends Controller
{
    public function index(Request $request)
    {
        $apps = QueryBuilder::for(Auth::user()->apps())
            ->defaultSort('-created_at')
            ->allowedFilters(['is_homework'])
            ->get();
        $apps->each->append('is_owned');
        return $apps;
    }

    public function store(Request $request)
    {
        return app(AppService::class)->store($request->all());
    }

    public function update(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppService::class)->update($app, $request->all());
    }

    public function show(Request $request, $app)
    {
        return QueryBuilder::for(BaseApp::class)
            ->allowedIncludes(['students'])
            ->findOrFail($app);
    }

    /*
     * Для тестирования. Выводит примеры генерируемых задач динамического тренажера*/
    public function getTaskList(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        echo json_encode($app->instance);
        echo '<br/><br/>';
        $rows = app(AppSessionService::class)->getTaskList($app, $request->all());

        foreach ($rows as $row) {
            echo $row;
            echo '<br/>';
        }
    }

    public function destroy(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppService::class)->destroy($app);
    }
}
