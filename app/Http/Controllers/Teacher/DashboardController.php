<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Teacher;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        /** @var Teacher $user */
        $user = Auth::user();
        $user->append('has_access');
        $user->load(['students' => function ($q) {
            $q->with(['lastSession', 'lastSubscription'])
                ->withCount('finishedSessions');
        }, 'groups.schedules']);
        $user->students->each->append(['average_quality', 'brains']);
        $user->students->makeHidden('finishedSessions');

        return [
            'user' => $user,
            'students' => $user->students,
            'groups' => $user->groups,
        ];
    }

}
