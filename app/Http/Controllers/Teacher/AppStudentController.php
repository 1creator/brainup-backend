<?php

namespace App\Http\Controllers\Teacher;

use App\Models\BaseApp;
use Laravel\Lumen\Routing\Controller;

class AppStudentController extends Controller
{
    public function index($app)
    {
        return BaseApp::findOrFail($app)
            ->students()
            ->with([
                'bestSession' => function ($q) use ($app) {
                    $q->where('app_id', $app);
                },
                'lastSession' => function ($q) use ($app) {
                    $q->where('app_id', $app);
                },
            ])
            ->get();
    }
}
