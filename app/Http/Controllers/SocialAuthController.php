<?php


namespace App\Http\Controllers;


use App\Models\Student;
use App\Services\SocialAuth\SocialAuthService;
use Illuminate\Http\Request;

class SocialAuthController
{
    public function redirect(Request $request)
    {
        return app(SocialAuthService::class)->getRedirect(Student::class, $request->all());
    }

    public function login(Request $request, $provider)
    {
        return app(SocialAuthService::class)->login(Student::class, $provider);
    }
}
