<?php


namespace App\Http\Controllers;


use App\Models\Admin;
use App\Models\Student;
use App\Models\Teacher;
use App\Services\Admin\AdminService;
use App\Services\Student\StudentService;
use App\Services\Teacher\TeacherService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController
{
    public function index(Request $request): ?\Illuminate\Contracts\Auth\Authenticatable
    {
        Validator::validate($request->all(), [
            'type' => 'required|in:admin,student,teacher'
        ]);
        $user = Auth::guard($request->get('type'))->user();
        if (!$user) {
            abort(403, 'Unauthorized');
        }
        switch (get_class($user)) {
            case Teacher::class:
                $user->load('image')
                    ->append(['has_access', 'level']);
                break;
            case Student::class:
                $user->load(['visualPreferences', 'image']);
                $user->append(['brains', 'has_access', 'level']);
                break;
        }
        return $user;
    }

    public function update(Request $request)
    {
        Validator::validate($request->all(), [
            'type' => 'required|in:admin,student,teacher'
        ]);
        $user = Auth::guard($request->get('type'))->user();
        if (!$user) {
            abort(403, 'Unauthorized');
        }
        switch (get_class($user)) {
            case Admin::class:
                return app(AdminService::class)->update($user, $request->all());
            case Teacher::class:
                return app(TeacherService::class)->update($user, $request->all());
            case Student::class:
                return app(StudentService::class)->update($user, $request->all());
        }
    }
}
