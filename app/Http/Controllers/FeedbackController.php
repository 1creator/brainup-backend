<?php


namespace App\Http\Controllers;


use App\Services\Feedback\FeedbackService;
use Illuminate\Http\Request;

class FeedbackController
{
    public function store(Request $request)
    {
        return app(FeedbackService::class)->store($request->all());
    }
}
