<?php

namespace App\Http\Controllers;

use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class UploadController extends Controller
{
    public function store(Request $request)
    {
        return app(AttachmentService::class)->store($request->file('file'), $request->type);
    }
}
