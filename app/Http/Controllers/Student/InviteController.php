<?php


namespace App\Http\Controllers\Student;


use App\Models\Student;
use App\Models\StudentInvite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InviteController
{
    public function index(Request $request)
    {
        /** @var Student $user */
        $user = Auth::guard('student')->user();
        return $user->invites;
    }

    public function accept(Request $request, $invite)
    {
        $invite = StudentInvite::findOrFail($invite);
        /** @var Student $user */
        $user = Auth::guard('student')->user();

        if ($invite->student_id != $user->id) {
            abort(401, 'Нет доступа к данному приглашению');
        }

        $user->group_id = $invite->group_id;
        $user->save();

        $invite->delete();

        return 1;
    }

    public function destroy(Request $request, $invite)
    {
        $invite = StudentInvite::findOrFail($invite);
        /** @var Student $user */
        $user = Auth::guard('student')->user();

        if ($invite->student_id != $user->id) {
            abort(401, 'Нет доступа к данному приглашению');
        }

        $invite->delete();
        return 1;
    }
}
