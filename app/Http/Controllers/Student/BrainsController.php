<?php


namespace App\Http\Controllers\Student;


use App\Models\Student;
use App\Services\ResetPassword\Services\ResetPasswordService;
use App\Services\SocialAuth\SocialAuthService;
use App\Services\Student\StudentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class BrainsController
{
    public function index(Request $request)
    {
        /** @var Student $user */
        $user = Auth::user();
        return $user->brainsHistory;
    }
}
