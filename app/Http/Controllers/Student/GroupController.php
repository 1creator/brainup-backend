<?php


namespace App\Http\Controllers\Student;


use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController
{
    public function index(Request $request)
    {
        /** @var Student $user */
        $user = Auth::guard('student')->user();
        $user->load(['group', 'group.students', 'group.teacher', 'group.schedules']);
        if (!$user->group) {
            abort(404);
        }
        return $user->group;
    }

    public function leave(Request $request)
    {
        /** @var Student $user */
        $user = Auth::guard('student')->user();
        $user->group()->dissociate()->save();
        return 1;
    }
}
