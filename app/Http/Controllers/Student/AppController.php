<?php

namespace App\Http\Controllers\Student;

use App\Models\BaseApp;
use App\Services\App\AppService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class AppController extends Controller
{
    public function index(Request $request)
    {
        $apps = QueryBuilder::for(Auth::user()->apps())
            ->defaultSort('-created_at')
            ->allowedIncludes(['userSessions'])
            ->allowedFilters(['is_homework'])
            ->get();
        $apps->each->append('is_owned')->append('userMaxQuality');
        return $apps;
    }

    public function store(Request $request)
    {
        return app(AppService::class)->store($request->all());
    }

    public function update(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppService::class)->update($app, $request->all());
    }

    public function show(Request $request, $app)
    {
        return QueryBuilder::for(BaseApp::class)
            ->allowedIncludes(['students'])
            ->findOrFail($app);
    }

    public function destroy(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);
        return app(AppService::class)->destroy($app);
    }
}
