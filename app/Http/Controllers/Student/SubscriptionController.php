<?php

namespace App\Http\Controllers\Student;

use App\Models\Teacher;
use App\Services\Subscription\SubscriptionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class SubscriptionController extends Controller
{
    public function transactions(Request $request)
    {
        /** @var Teacher $teacher */
        $teacher = Auth::user();
        return QueryBuilder::for($teacher->subscriptionTransactions()->getQuery())->get();
    }

    public function buySubscription(Request $request)
    {
        return app(SubscriptionService::class)->getRedirectForStudentSubscriptionBuy($request->all());
    }
}
