<?php


namespace App\Http\Controllers\Student;


use App\Models\BrainValue;
use App\Models\Student;
use App\Services\Student\QueryBuilder\StudentBrainsSort;
use App\Utils\QueryBuilder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\AllowedSort;

class StudentController
{
    public function index(Request $request)
    {
//        return Student::query()
//            ->orderByDesc(BrainValue::select('value')
//                ->whereColumn('brain_values.student_id', 'students.id')
//                ->orderByDesc('value')
//                ->take(1)
//            )
//            ->select(['students.*', 'brain_values.value'])
//            ->get();


//        return Student::query()
//            ->leftJoin('brain_values', function (JoinClause $join) {
//                $join->on('brain_values.student_id', '=', 'students.id')
//                    ->on('brain_values.date', '=', DB::raw('(SELECT MAX(brain_values.date) FROM brain_values WHERE student_id = students.id)'));
//            })
//            ->select(['students.*', 'brain_values.value as brains'])
//            ->groupBy('students.id')
//            ->get();


        return QueryBuilder::for(Student::class)
            ->allowedSorts(AllowedSort::custom('brains', new StudentBrainsSort()))
            ->get();
    }
}
