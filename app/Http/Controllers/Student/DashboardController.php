<?php

namespace App\Http\Controllers\Student;

use App\Models\BrainValue;
use App\Models\Student;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        /** @var Student $user */
        $user = Auth::user();
        $user->load([
            'visualPreferences',
            'group.teacher',
            'group.schedules',
            'group.students.lastBrains',
        ]);
        if ($user->group) {
            $user->group->students->each->append(['brains']);
        }
        $user->append(['brains', 'has_access']);
        return [
            'user' => $user,
            'group' => $user->group,
        ];
    }

}
