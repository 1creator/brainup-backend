<?php

namespace App\Http\Controllers\Student;

use App\Services\Attestation\AttestationService;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class AttestationStageController extends Controller
{

    public function show(Request $req, $stage)
    {
        return app(AttestationService::class)->getStage($stage, $req->get('start'));
    }

    public function answer(Request $req, $stage)
    {
        return app(AttestationService::class)->answerStage($stage, $req->get('answers'));
    }
}
