<?php

namespace App\Http\Controllers\Student;

use App\Models\BaseApp;
use App\Models\BrainValue;
use App\Models\Student;
use App\Services\App\AppService;
use App\Utils\QueryBuilder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;

class LeaderBoardController extends Controller
{
    public function index(Request $request)
    {
        $students = Student::query()
            ->leftJoin('brain_values', function (JoinClause $join) {
                $join->on('brain_values.student_id', '=', 'students.id')
                    ->on('brain_values.date', '=', DB::raw('(SELECT MAX(brain_values.date) FROM brain_values WHERE student_id = students.id)'));
            })
            ->select(['students.*', 'brain_values.value as brains'])
            ->orderByDesc('brains')
            ->take(10)
            ->get();

        /** @var Student $user */
        $user = Auth::user();
        $user->append('brains');

        $students->transform(function ($item, $idx) {
            $item['position'] = $idx + 1;
            return $item;
        });

        if (!$students->find($user)) {
            $user['position'] = BrainValue::where('value', '>=', $user['brains'])->distinct('student_id')->count() + 1;
            $students->push($user);
        }

        return $students;
    }
}
