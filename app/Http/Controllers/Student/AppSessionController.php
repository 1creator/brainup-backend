<?php

namespace App\Http\Controllers\Student;

use App\Models\AppSession;
use App\Models\BaseApp;
use App\Models\Student;
use App\Services\App\AppSessionService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller;

class AppSessionController extends Controller
{
    public function index()
    {
        /** @var Student $user */
        $user = Auth::user();
        return QueryBuilder::for($user->sessions())
            ->allowedFilters(['app_id', 'finished'])
            ->defaultSort('-updated_at')
            ->with(['app'])
            ->get()
            ->append('app_type');
    }

    public function play(Request $request, $app)
    {
        $app = BaseApp::findOrFail($app);

        /** @var Student $student */
        $student = Auth::user();

        if (!$student->has_access && !$app->is_free) {
            abort(401, 'Для прохождения этого тренажёра необходима подписка.');
        }

        return app(AppSessionService::class)->play($app, $request->all());
    }

    public function show(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);

        /** @var Student $student */
        $student = Auth::user();

        if (!$student->has_access && !$appSession->app->is_free) {
            abort(401, 'Для прохождения этого тренажёра необходима подписка.');
        }

        $appSession->append('app_type');
        return $appSession;
    }

    public function restart(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->restart($appSession, $request->all());
    }

    public function updateSettings(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        if ($appSession->app->is_free || $appSession->app->is_homework) {
            abort(401, 'Настройки этого тренажера нельзя менять во время прохождения.');
        }

        return app(AppSessionService::class)->updateSettings($appSession, $request->all());
    }

    public function storeAnswer(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->answer($appSession, $request->all());
    }

    public function getTask(Request $request, $appSession)
    {
        $appSession = AppSession::findOrFail($appSession);
        return app(AppSessionService::class)->getTask($appSession);
    }
}
