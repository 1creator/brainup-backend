<?php

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $level
 * @property \Illuminate\Database\Eloquent\Model|mixed $certificate
 */
class UserAttestationLevel extends Model
{
    protected $table = 'user_attestation_level';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $with = ['certificate'];

    public function user(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function certificate(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }
}
