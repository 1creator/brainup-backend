<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * @method static create(array $data)
 * @method static findOrFail($invite)
 */
class Variable extends Model
{
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];
}
