<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property \Illuminate\Contracts\Auth\Authenticatable|mixed|null $user
 * @property mixed $id
 * @property mixed $score
 * @property mixed $stages
 * @property bool|mixed $finished_at
 * @property mixed $level
 * @property mixed $type
 * @property mixed $is_training
 * @property mixed $stages_sum_score
 * @property mixed $score_percent
 */
class Attestation extends Model
{
    protected $table = 'attestations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $appends = ['score_percent', 'localized_type'];

    public function user(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function stages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(AttestationStage::class, 'attestation_id');
    }

    public function attestationLevelDetails()
    {
        return $this->hasOne(UserAttestationLevel::class, 'attestation_id');
    }

    public function getLocalizedTypeAttribute()
    {
        return [
            'abakus' => 'Абакус',
            'mental' => 'Ментально',
        ][$this->type];
    }

    public function getScorePercentAttribute()
    {
        if (!$this->stages_sum_score) {
            return 0;
        }

        if ($this->type == 'mental') {
            $maxScore = $this->level > 6 ? 100 : 300;
        } else {
            $maxScore = $this->level > 8 ? 150 : 350;
        }
        return round((int)$this->stages_sum_score / $maxScore, 2);
    }
}
