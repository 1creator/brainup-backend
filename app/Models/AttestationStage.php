<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array|mixed $user_answers
 * @property mixed $answers
 * @property mixed $score
 * @property mixed $finished_at
 * @property mixed $attestation
 * @property mixed $attestation_id
 * @property Carbon|mixed $started_at
 * @property mixed $type
 */
class AttestationStage extends Model
{
    protected $table = 'attestation_stages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'tasks' => 'array',
        'answers' => 'array',
        'user_answers' => 'array',
    ];

    protected $dates = ['started_at', 'finished_at', 'created_at', 'updated_at'];

    protected $hidden = ['tasks', 'answers'];

    public function attestation(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Attestation::class);
    }
}
