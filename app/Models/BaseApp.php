<?php

namespace App\Models;

use App\Services\App\BaseAppService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property Model|DynamicApp instance
 * @property mixed instance_type
 * @property mixed type
 * @property mixed|string invite_token
 * @property mixed id
 * @method static findOrFail($app)
 */
class BaseApp extends Model
{
    protected $table = 'apps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $with = ['instance'];
    protected $appends = ['type'];

    public function getTypeAttribute()
    {
        if (defined($this->instance_type . '::ALIAS')) {
            return ($this->instance_type)::ALIAS;
        } else {
            return $this->instance_type;
        }

//        return [
//                StaticApp::class => "static",
//                DynamicApp::class => "dynamic",
//                FlashApp::class => "flash",
//                MultiplicationApp::class => "multiplication"
//            ][$this->instance_type] ?? $this->instance_type;
    }

    public function getService(): BaseAppService
    {
        return app('apps.' . $this->type);
    }

    public function getIsOwnedAttribute()
    {
        $user = Auth::user();
        return $user &&
            $this->author_id == $user->id &&
            $this->author_type == get_class($user);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'student_app',
            'app_id', 'student_id');
    }

    public function userSessions()
    {
        $user = Auth::user();
        return $this->sessions()
            ->where('player_type', get_class($user))
            ->where('player_id', $user->id);
    }

    public function bestSession()
    {
        return $this->hasOne(AppSession::class, 'app_id')
            ->orderBy('quality', 'desc');
    }

    public function lastSession()
    {
        return $this->hasOne(AppSession::class, 'app_id')
            ->orderBy('updated_at', 'desc');
    }

    public function getUserMaxQualityAttribute()
    {
        return round($this->userSessions()->max('quality') ?? 0);
    }

    public function sessions()
    {
        return $this->hasMany(AppSession::class, 'app_id');
    }

    public function instance()
    {
        return $this->morphTo('instance');
    }

    public function author()
    {
        return $this->morphTo('author');
    }

    public function getIsFreeAttribute(): bool
    {
        return DB::table('free_apps')->where('app_id', $this->id)->exists();
    }
}
