<?php

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Services\Subscription\HasSubscription;
use App\Services\Subscription\HasSubscriptions;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;


/**
 * @method static create(array $data)
 * @method static findOrFail($invite)
 */
class StudentInvite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    public const UPDATED_AT = null;

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
