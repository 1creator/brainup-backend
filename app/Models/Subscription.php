<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static orderByDesc(string $string)
 * @property Carbon expired_at
 * @property bool is_expired
 */
class Subscription extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    protected $dates = ['expired_at'];

    public function user()
    {
        return $this->morphTo('user');
    }

    public function getIsExpiredAttribute()
    {
        return Carbon::now()->isAfter($this->expired_at);
    }
}
