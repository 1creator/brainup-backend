<?php

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Services\Attestation\HasAttestations;
use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Traits\InteractsWithPaymentsTrait;
use App\Services\Subscription\HasSubscription;
use App\Services\Subscription\HasSubscriptions;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;

/**
 * @property mixed sessions
 * @property mixed students
 * @property mixed|string password
 * @property mixed defaultGroup
 * @property mixed last_name
 * @property mixed first_name
 * @property mixed short_name
 * @property mixed subscriptions_left
 * @property mixed middle_name
 * @property mixed full_name
 * @property mixed id
 * @property UserAttestationLevel $attestationLevel
 * @method static findOrFail($teacher)
 */
class Teacher extends Model implements AuthenticatableContract, AuthorizableContract, HasSubscription, InteractsWithPaymentsInterface
{
    use Authenticatable, Authorizable, HasSubscriptions, Notifiable, InteractsWithPaymentsTrait, HasAttestations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'password', 'api_token'
    ];

    protected $dates = ['birthday'];

    protected $attributes = [
        'subscriptions_left' => 0,
    ];

    protected $appends = ['full_name', 'account_type'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token'
    ];

    protected $with = ['image'];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->api_token = Str::random(32);
        });
        self::created(function (Teacher $model) {
            $model->createDefaultGroup();
        });
    }

    public function students()
    {
        return $this->hasManyThrough(Student::class, Group::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function sessions()
    {
        return $this->morphMany(AppSession::class, 'player');
    }

    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }

    public function apps()
    {
        return $this->morphMany(BaseApp::class, 'author');
    }

    public function defaultGroup()
    {
        return $this->hasOne(Group::class)->where(['name' => 'Нераспределенные ученики']);
    }

    public function createDefaultGroup()
    {
        if (!$this->defaultGroup)
            $this->groups()->create(['name' => 'Нераспределенные ученики']);
    }

    public function getShortNameAttribute()
    {
        $res = $this->last_name;
        if ($this->first_name) {
            $res .= ' ' . mb_substr($this->first_name, 0, 1) . '.';
        }
        if ($this->middle_name) {
            $res .= ' ' . mb_substr($this->middle_name, 0, 1) . '.';
        }
        return $res;
    }

    public function getFullNameAttribute()
    {
        $res = $this->last_name;
        if ($this->first_name) {
            $res .= ' ' . $this->first_name;
        }
        if ($this->middle_name) {
            $res .= ' ' . $this->middle_name;
        }
        return $res;
    }

    public function getAccountTypeAttribute()
    {
        return 'Преподаватель';
    }
}
