<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed task_count
 * @property mixed task_size
 * @property mixed digits_capacity
 * @property mixed operations
 * @property mixed operation_alt
 * @property mixed bit_alt
 * @property mixed fraction
 * @property mixed mirror
 * @property mixed negative
 * @property mixed formulas
 * @property mixed watch_time_progress
 * @property mixed watch_time
 * @property mixed task_size_progress
 * @property mixed mode
 */
class DynamicApp extends Model
{
    const ALIAS = 'dynamic';
    protected $table = 'dynamic_digits_apps';
    public $timestamps = null;
    protected $attributes = [
        'digits_capacity' => "[1, 2]",
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $casts = ['digits_capacity' => 'array'];
}
