<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array tasks
 * @property array answers
 * @property array state
 * @property bool|mixed finished
 * @property float|int|mixed quality
 * @property BaseApp app
 * @property mixed min_watch_time
 * @property mixed settings
 * @property mixed|string player_type
 * @property mixed|string player_id
 * @property mixed bit_formulas
 * @property mixed disabled_mixed_formulas
 * @property mixed step
 * @property mixed app_type
 * @property mixed brainsPerTask
 * @method static findOrFail($appSession)
 */
class AppSession extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'tasks' => 'array',
        'answers' => 'array',
        'state' => 'array',
        'settings' => 'array',
    ];

    protected $attributes = [
        'tasks' => "[]",
        'answers' => "[]",
        'state' => "[]",
        'settings' => "[]",
        'quality' => 0,
    ];

    protected $appends = ['step'];

//    protected $hidden = ['answers'];

    public function app()
    {
        return $this->belongsTo(BaseApp::class, 'app_id');
    }

    public function player()
    {
        return $this->morphTo('player');
    }

    public function calcQuality()
    {
        $res = 0;
        $pointsPerTask = 100 / $this->settings['task_count'];
        for ($i = 0; $i < $this->step; $i++) {
            $res += ($this->answers[$i] == $this->state['user_answers'][$i]) ? $pointsPerTask : 0;
        }
        return $res;
    }

    /**
     * Удалить задачи, на которые пользователь ещё не ответил
     */
    public function flushTasks()
    {
        $userAnswersCount = $this->step;

        $this->tasks = array_slice($this->tasks, 0, $userAnswersCount);
        $this->answers = array_slice($this->answers, 0, $userAnswersCount);
    }

    public function isFinished()
    {
        return count($this->state['user_answers'] ?? []) >= $this->settings['task_count'];
    }

    public function getStepAttribute()
    {
        return count($this->state['user_answers'] ?? []);
    }

    public function getAppTypeAttribute()
    {
        return $this->app->type;
    }

    public function getBrainsPerTaskAttribute()
    {
        return $this->app->getService()->getBrainsPerTask($this);
    }
}
