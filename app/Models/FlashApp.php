<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array digits_capacity
 */
class FlashApp extends Model
{
    const ALIAS = 'flash';
    protected $table = 'flash_apps';
    public $timestamps = null;

    protected $attributes = [
        'digits_capacity' => "[1, 2]",
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    protected $casts = ['digits_capacity' => 'array'];
}
