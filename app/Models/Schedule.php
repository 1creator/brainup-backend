<?php


namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Schedule
 *
 * @property int $id
 * @property int $group_id
 * @property int $mon
 * @property int $tue
 * @property int $wed
 * @property int $thu
 * @property int $fri
 * @property int $sat
 * @property int $sun
 * @property string|null $date
 * @property string $start_at
 * @property string|null $finish_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Group $group
 * @method static Builder|Schedule newModelQuery()
 * @method static Builder|Schedule newQuery()
 * @method static Builder|Schedule query()
 * @method static Builder|Schedule whereCreatedAt($value)
 * @method static Builder|Schedule whereDate($value)
 * @method static Builder|Schedule whereFinishAt($value)
 * @method static Builder|Schedule whereFri($value)
 * @method static Builder|Schedule whereGroupId($value)
 * @method static Builder|Schedule whereId($value)
 * @method static Builder|Schedule whereMon($value)
 * @method static Builder|Schedule whereSat($value)
 * @method static Builder|Schedule whereStartAt($value)
 * @method static Builder|Schedule whereSun($value)
 * @method static Builder|Schedule whereThu($value)
 * @method static Builder|Schedule whereTue($value)
 * @method static Builder|Schedule whereUpdatedAt($value)
 * @method static Builder|Schedule whereWed($value)
 * @mixin Eloquent
 */
class Schedule extends Model
{
    protected $table = 'group_schedules';
    protected $guarded = ['id'];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

}
