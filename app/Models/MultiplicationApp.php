<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array digits_capacity
 */
class MultiplicationApp extends Model
{
    const ALIAS = 'multiplication';
    protected $table = 'multiplication_apps';
    public $timestamps = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];
}
