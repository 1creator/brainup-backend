<?php

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Services\Attestation\HasAttestations;
use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Traits\InteractsWithPaymentsTrait;
use App\Services\Subscription\HasSubscription;
use App\Services\Subscription\HasSubscriptions;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;

/**
 * @property mixed sessions
 * @property VisualPreferences visualPreferences
 * @property mixed finishedSessions
 * @property mixed|string password
 * @property mixed group_id
 * @property mixed brainsToday
 * @property mixed id
 * @property mixed last_name
 * @property mixed middle_name
 * @property mixed first_name
 * @property mixed short_name
 * @property mixed invites
 * @property mixed has_access
 * @property Group $group
 * @property UserAttestationLevel $attestationLevel
 * @method static findOrFail($student)
 * @method static where(string $string, $email)
 * @method static select(string $string)
 */
class Student extends Model implements AuthenticatableContract, AuthorizableContract, HasSubscription,
    InteractsWithPaymentsInterface
{
    use Authenticatable, Authorizable, HasSubscriptions, Notifiable, InteractsWithPaymentsTrait, HasAttestations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'password', 'api_token'
    ];

    protected $appends = ['full_name', 'account_type'];

    protected $dates = ['birthday'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token'
    ];

    protected $with = ['image'];

    public function getBrainsAttribute()
    {
        return $this->lastBrains->value ?? 0;
    }

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->api_token = Str::random(32);
        });
    }

    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function sessions()
    {
        return $this->morphMany(AppSession::class, 'player');
    }

    public function bestSession()
    {
        return $this->morphOne(AppSession::class, 'player')
            ->orderBy('quality', 'desc')->take(1);
    }

    public function lastSession()
    {
        return $this->morphOne(AppSession::class, 'player')
            ->orderBy('updated_at', 'desc');
    }

    public function apps()
    {
        return $this->belongsToMany(BaseApp::class, 'student_app',
            'student_id', 'app_id');
    }

    public function getAppsWithStatsAttribute()
    {
        // todo fix
        return $this->apps()->with([
            'bestSession' => function ($q) {
                $q->where('player_id', $this->id)->where('player_type', Student::class);
            },
            'lastSession' => function ($q) {
                $q->where('player_id', $this->id)->where('player_type', Student::class);
            },
        ])->get();
    }

    public function getAverageQualityAttribute()
    {
        return round($this->finishedSessions()->avg('quality') ?? 0);
    }

    public function finishedSessions()
    {
        return $this->sessions()->where('finished', true);
    }

    public function brainsHistory()
    {
        return $this->hasMany(BrainValue::class);
    }

    public function brainsToday()
    {
        return $this->hasMany(BrainValue::class)->whereDate('date', Carbon::now());
    }

//    public function lastBrains()
//    {
//        return $this->hasOne(BrainValue::class)->orderByDesc('date')->take(1);
//    }

    public function lastBrains()
    {
        return $this->hasOne(BrainValue::class)->orderByDesc('date');
    }

    public function visualPreferences()
    {
        return $this->hasOne(VisualPreferences::class, 'student_id')->withDefault([
            'color_scheme' => 'standard',
            'interface_scale' => 'standard',
            'pictures' => true,
        ]);
    }

    public function getShortNameAttribute()
    {
        $res = $this->last_name;
        if ($this->first_name) {
            $res .= ' ' . mb_substr($this->first_name, 0, 1) . '.';
        }
        if ($this->middle_name) {
            $res .= ' ' . mb_substr($this->middle_name, 0, 1) . '.';
        }
        return $res;
    }

    public function invites()
    {
        return $this->hasMany(StudentInvite::class)->with('group.teacher');
    }

    public function getFullNameAttribute()
    {
        $res = $this->last_name;
        if ($this->first_name) {
            $res .= ' ' . $this->first_name;
        }
        if ($this->middle_name) {
            $res .= ' ' . $this->middle_name;
        }
        return $res;
    }

    public function getAccountTypeAttribute()
    {
        return 'Ученик';
    }
}
