<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisualPreferences extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    public $timestamps = null;

    protected $table = 'student_visual_preferences';

    protected $hidden = ['student_id', 'id'];
}
