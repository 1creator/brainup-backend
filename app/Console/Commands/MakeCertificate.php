<?php

namespace App\Console\Commands;

use App\Services\Variables\VariablesService;
use Illuminate\Console\Command;
use Imagick;
use ImagickDraw;
use ImagickPixel;

class MakeCertificate extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:certificate {--level=} {--fio=} {--date=}';

    /**
     * The console command description.
     *
     * @var void
     */
    protected $description = "Создать сертификат прохождения аттестации.";

    public function handle(): void
    {
        $template = app(VariablesService::class)->get('attestationCertificateTemplate');
        if (!$template) {
            $this->error('Не найден шаблон сертификата');
            abort(404);
        }

        $image = new Imagick($template->url);
        $imageWidth = $image->getImageWidth();
        $imageHeight = $image->getImageHeight();

        $fioX = $imageWidth * 0.5;
        $fioY = $imageHeight * 0.49;

        $levelX = $imageWidth * 0.585;
        $levelY = $imageHeight * 0.57;

        $dateX = $imageWidth * 0.69;
        $dateY = $imageHeight * 0.78;

        $fio = $this->option('fio');
        $level = $this->option('level');
        $date = $this->option('date');

        $draw = new ImagickDraw();
        $draw->setFillColor(new ImagickPixel('black'));
        $draw->setFontSize(45);
        $draw->setTextAlignment(imagick::ALIGN_CENTER);
        $image->annotateImage($draw, $fioX, $fioY, 0, $fio);
        $image->annotateImage($draw, $levelX, $levelY, 0, $level);
        $image->annotateImage($draw, $dateX, $dateY, 0, $date);
        $image->setImageFormat('png');

        $path = 'certificates/' . $level . '-' . $fio . '.png';
        $image->writeImage(storage_path('app/public/' . $path));
        $this->info($path);
    }
}
