<?php

use App\Http\Middleware\CorsMiddleware;
use App\Services\Admin\AdminService;
use App\Services\App\AppService;
use App\Services\App\AppSessionService;
use App\Services\App\DynamicAppService;
use App\Services\App\FlashAppService;
use App\Services\App\MultiplicationAppService;
use App\Services\App\StaticAppService;
use App\Services\Attachment\Providers\AttachmentProvider;
use App\Services\Attachment\Services\AttachmentService;
use App\Services\Group\GroupService;
use App\Services\Payment\Providers\PaymentProvider;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Notifications\NotificationServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use SocialiteProviders\Manager\ServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__)
))->bootstrap();

date_default_timezone_set(env('APP_TIMEZONE', 'UTC'));

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();

$app->withEloquent();

JsonResource::withoutWrapping();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Config Files
|--------------------------------------------------------------------------
|
| Now we will register the "app" configuration file. If the file exists in
| your configuration directory it will be loaded; otherwise, we'll load
| the default version. You may register other files below as needed.
|
*/

//$app->configure('app');
URL::forceRootUrl(config('app.url'));
if (Str::startsWith(config('app.url'), 'https')) {
    URL::forceScheme('https');
}


$app->configure('services');
$app->register(Spatie\QueryBuilder\QueryBuilderServiceProvider::class);
$app->register(\Spatie\DbSnapshots\DbSnapshotsServiceProvider::class);
//$app->register(SocialiteServiceProvider::class);
//$app->configure('query-builder');
/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    CorsMiddleware::class
]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

//$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(AttachmentProvider::class);
$app->register(PaymentProvider::class);
$app->register(NotificationServiceProvider::class);
$app->register(ServiceProvider::class);
$app->bind(AdminService::class);
$app->bind(GroupService::class);
$app->bind(AttachmentService::class);

$app->bind(AppService::class);
$app->bind('apps.dynamic', DynamicAppService::class);
$app->bind('apps.static', StaticAppService::class);
$app->bind('apps.flash', FlashAppService::class);
$app->bind('apps.multiplication', MultiplicationAppService::class);
$app->bind(AppSessionService::class);

$app->register(MailServiceProvider::class);
$app->configure('mail');
$app->alias('mail.manager', Illuminate\Mail\MailManager::class);
$app->alias('mail.manager', Illuminate\Contracts\Mail\Factory::class);
$app->alias('mailer', Illuminate\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\MailQueue::class);

//$app->register(App\Providers\AuthServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

app('translator')->setLocale('ru');
Carbon::setLocale('ru');

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__ . '/../routes/web.php';
});

return $app;
