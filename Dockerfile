FROM 1creator/php
COPY ./ /var/www/
WORKDIR /var/www
RUN composer install
RUN apt-get --allow-releaseinfo-change update && apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-enable imagick
