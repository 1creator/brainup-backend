
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Volkov','Aleksandr',NULL,'$2y$10$68lZUKARKw6pLMQXWfEsy.KzU9001.VFZTOij1c9F5u4plcsf0XJ6','tBU85SCZCJ8FH9TUe1gwn80PCPlySkGo','admin@1creator.ru',NULL,'2020-11-21 17:46:54','2020-11-21 17:46:54');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `app_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_sessions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `app_id` bigint unsigned NOT NULL,
  `player_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_id` bigint unsigned NOT NULL,
  `settings` json NOT NULL,
  `tasks` json NOT NULL,
  `answers` json NOT NULL,
  `state` json DEFAULT NULL,
  `quality` double(8,2) NOT NULL DEFAULT '0.00',
  `finished` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_sessions_app_id_foreign` (`app_id`),
  KEY `app_sessions_player_type_player_id_index` (`player_type`,`player_id`),
  CONSTRAINT `app_sessions_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `app_sessions` WRITE;
/*!40000 ALTER TABLE `app_sessions` DISABLE KEYS */;
INSERT INTO `app_sessions` VALUES (1,1,'App\\Models\\Student',1,'{\"id\": 1, \"mode\": \"digits\", \"mirror\": 0, \"bit_alt\": 0, \"formulas\": \"all\", \"fraction\": 0, \"negative\": 0, \"task_size\": 3, \"horizontal\": 0, \"input_time\": 10, \"operations\": \"mixed\", \"task_count\": 20, \"watch_time\": 5, \"bit_formulas\": \"all\", \"operation_alt\": 0, \"min_watch_time\": 0.5, \"digits_capacity\": [1, 1], \"without_cross_in\": \"none\", \"task_size_progress\": 0, \"watch_time_progress\": 0, \"disable_mixed_formulas\": 0}','[{\"answer\": 9, \"addendums\": [0, 0, 9], \"watch_time\": 5, \"combinations\": false}, {\"answer\": 1, \"addendums\": [4, -4, 1], \"watch_time\": 5, \"combinations\": false}]','[9, 1]','{\"user_answers\": [\"9\", \"1\"]}',10.00,0,'2020-11-24 15:53:51','2020-11-24 15:54:41'),(2,1,'App\\Models\\Student',1,'{\"id\": 1, \"mode\": \"digits\", \"mirror\": 0, \"bit_alt\": 0, \"formulas\": \"all\", \"fraction\": 0, \"negative\": 0, \"task_size\": 3, \"horizontal\": 0, \"input_time\": 10, \"operations\": \"mixed\", \"task_count\": 20, \"watch_time\": 5, \"bit_formulas\": \"all\", \"operation_alt\": 0, \"min_watch_time\": 0.5, \"digits_capacity\": [1, 1], \"without_cross_in\": \"none\", \"task_size_progress\": 0, \"watch_time_progress\": 0, \"disable_mixed_formulas\": 0}','[{\"answer\": 14, \"addendums\": [7, 1, 6], \"watch_time\": 5, \"combinations\": false}]','[14]','{\"user_answers\": [\"14\"]}',5.00,0,'2020-11-24 15:54:59','2020-11-24 15:55:11'),(3,2,'App\\Models\\Teacher',1,'{\"id\": 4, \"mode\": \"digits\", \"mirror\": 0, \"bit_alt\": 0, \"formulas\": \"ten\", \"fraction\": 0, \"negative\": 0, \"task_size\": 10, \"input_time\": 0, \"operations\": \"mixed\", \"task_count\": 50, \"watch_time\": 1, \"bit_formulas\": \"all\", \"operation_alt\": false, \"min_watch_time\": 0.5, \"digits_capacity\": [1, 1], \"without_cross_in\": \"none\", \"task_size_progress\": 0, \"watch_time_progress\": 0, \"disable_mixed_formulas\": 0}','[{\"answer\": 5, \"addendums\": [4, -1, 3, -4, 3], \"watch_time\": 1}, {\"answer\": 14, \"addendums\": [8, -4, 6, 8, -4], \"watch_time\": 1}, {\"answer\": 5, \"addendums\": [1, -1, 3, -3, 5], \"watch_time\": 1}, {\"answer\": 6, \"addendums\": [3, -2, 4, -2, 3], \"watch_time\": 1}, {\"answer\": 16, \"addendums\": [4, -2, 8, -1, 7], \"watch_time\": 1}, {\"answer\": 10, \"addendums\": [1, -1, 8, -5, 7], \"watch_time\": 1}, {\"answer\": 10, \"addendums\": [2, -1, 9, 0, 0], \"watch_time\": 1}, {\"answer\": 2, \"addendums\": [2, -2, 1, -1, 2], \"watch_time\": 1}, {\"answer\": 11, \"addendums\": [7, -3, 7, -9, 9], \"watch_time\": 1}, {\"answer\": 10, \"addendums\": [2, -2, 2, -1, 9], \"watch_time\": 1}, {\"answer\": 18, \"addendums\": [3, -2, 9, -1, 9], \"watch_time\": 1}, {\"answer\": 22, \"addendums\": [3, 9, 8, -3, -6, -7, -2, 8, 7, 5], \"watch_time\": 1}]','[5, 14, 5, 6, 16, 10, 10, 2, 11, 10, 18, 22]','{\"user_answers\": [\"5\", \"14\", \"\", \"6\", \"16\", \"10\", \"10\", \"\", \"\", \"\", \"18\"]}',14.00,0,'2020-12-22 09:00:25','2020-12-22 10:59:32'),(4,3,'App\\Models\\Teacher',1,'{\"id\": 3, \"mode\": \"digits\", \"mirror\": 0, \"bit_alt\": 0, \"formulas\": \"ten\", \"fraction\": 0, \"negative\": 0, \"task_size\": 5, \"horizontal\": 0, \"input_time\": 10, \"operations\": \"mixed\", \"task_count\": 20, \"watch_time\": 5, \"bit_formulas\": \"all\", \"operation_alt\": 1, \"min_watch_time\": 0.5, \"digits_capacity\": [1, 1], \"without_cross_in\": \"none\", \"task_size_progress\": 0, \"watch_time_progress\": 0, \"disable_mixed_formulas\": 0}','[{\"answer\": 13, \"addendums\": [5, -3, 8, 5, -2], \"watch_time\": 5, \"combinations\": false}, {\"answer\": 6, \"addendums\": [2, -2, 9, -7, 4], \"watch_time\": 5, \"combinations\": false}, {\"answer\": 18, \"addendums\": [9, -2, 3, -1, 9], \"watch_time\": 5, \"combinations\": false}]','[13, 6, 18]','{\"user_answers\": [\"\", \"\"]}',0.00,0,'2020-12-22 13:51:03','2020-12-22 13:59:58'),(5,2,'App\\Models\\Teacher',1,'{\"id\": 1, \"mode\": \"digits\", \"mirror\": 0, \"bit_alt\": 0, \"formulas\": \"all\", \"fraction\": 0, \"negative\": 0, \"task_size\": 8, \"input_time\": 0, \"operations\": \"mixed\", \"task_count\": 50, \"watch_time\": 1, \"bit_formulas\": \"all\", \"operation_alt\": 1, \"min_watch_time\": 0.5, \"digits_capacity\": [1, 1], \"without_cross_in\": \"none\", \"task_size_progress\": 0, \"watch_time_progress\": 0, \"disable_mixed_formulas\": 0}','[{\"answer\": 18, \"addendums\": [3, -1, 8, 5, -2, 7, -5, 3], \"watch_time\": 1}]','[18]','[]',0.00,0,'2020-12-22 14:00:04','2020-12-22 14:00:07');
/*!40000 ALTER TABLE `app_sessions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invite_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` bigint unsigned NOT NULL,
  `instance_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance_id` bigint unsigned NOT NULL,
  `min_quality` double(8,2) NOT NULL DEFAULT '0.75',
  `is_homework` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_author_type_author_id_index` (`author_type`,`author_id`),
  KEY `apps_instance_type_instance_id_index` (`instance_type`,`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `apps` WRITE;
/*!40000 ALTER TABLE `apps` DISABLE KEYS */;
INSERT INTO `apps` VALUES (1,'Статический тренажер','n35QiJyxJDjOAAqK','App\\Models\\Teacher',1,'App\\Models\\StaticApp',1,70.00,1,'2020-11-24 15:53:39','2020-11-24 15:56:46'),(2,'Динамический тренажер','hLwp3qi6zWEbWUpN','App\\Models\\Teacher',1,'App\\Models\\DynamicApp',1,0.75,0,'2020-12-22 09:00:17','2020-12-22 09:00:17'),(3,'Статический тренажер','X2DG5yLLXSlP6YEK','App\\Models\\Teacher',1,'App\\Models\\StaticApp',2,0.75,0,'2020-12-22 13:50:56','2020-12-22 13:50:56');
/*!40000 ALTER TABLE `apps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public',
  `original` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint unsigned DEFAULT NULL,
  `order_column` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachable_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attachments_attachable_type_attachable_id_index` (`attachable_type`,`attachable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `attachments` WRITE;
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
INSERT INTO `attachments` VALUES (1,'pTbKNgT3YSA.jpg','public','24fOdMCiZzG0shH5Zt3vXHSkYySBphK2KZsgHAko.jpeg',NULL,'image/jpeg',NULL,438643,NULL,'2020-12-05 19:44:11',NULL,'App\\Models\\Student',2),(2,'DgB3LcggjRg.jpg','public','77Fe98FpBsa9vh7jRhJswssP2JDD1iQqhOB1LlRo.jpeg',NULL,'image/jpeg',NULL,492583,NULL,'2020-12-05 19:46:39',NULL,NULL,NULL);
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `brain_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brain_values` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `student_id` bigint unsigned NOT NULL,
  `value` decimal(12,2) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brain_values_student_id_foreign` (`student_id`),
  KEY `brain_values_date_index` (`date`),
  CONSTRAINT `brain_values_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brain_values` WRITE;
/*!40000 ALTER TABLE `brain_values` DISABLE KEYS */;
INSERT INTO `brain_values` VALUES (1,1,1.80,'2020-11-24'),(2,2,0.75,'2020-11-24'),(3,1,3.00,'2020-11-25');
/*!40000 ALTER TABLE `brain_values` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dynamic_digits_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_digits_apps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `mode` enum('digits','combinations','mixed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'digits',
  `operations` enum('sum','sub','mixed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mixed',
  `operation_alt` tinyint(1) NOT NULL DEFAULT '0',
  `bit_alt` tinyint(1) NOT NULL DEFAULT '0',
  `without_cross_in` enum('none','10','50','100','500','1000') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `bit_formulas` enum('all','except_up') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `formulas` enum('none','all','five','ten') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `digits_capacity` json NOT NULL,
  `fraction` int NOT NULL DEFAULT '0',
  `negative` tinyint(1) NOT NULL DEFAULT '0',
  `mirror` tinyint(1) NOT NULL DEFAULT '0',
  `task_count` int NOT NULL DEFAULT '20',
  `task_size` int NOT NULL DEFAULT '5',
  `task_size_progress` tinyint(1) NOT NULL DEFAULT '0',
  `watch_time` double(8,2) NOT NULL DEFAULT '2.00',
  `watch_time_progress` tinyint(1) NOT NULL DEFAULT '0',
  `min_watch_time` double(8,2) NOT NULL DEFAULT '0.50',
  `disable_mixed_formulas` tinyint(1) NOT NULL DEFAULT '0',
  `input_time` double(8,2) NOT NULL DEFAULT '10.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dynamic_digits_apps` WRITE;
/*!40000 ALTER TABLE `dynamic_digits_apps` DISABLE KEYS */;
INSERT INTO `dynamic_digits_apps` VALUES (1,'digits','mixed',1,0,'none','all','all','[1, 1]',0,0,0,50,8,0,1.00,0,0.50,0,0.00),(2,'digits','mixed',1,0,'none','all','ten','[1, 1]',0,0,0,50,5,0,1.00,0,0.50,0,0.00),(3,'digits','mixed',1,0,'none','all','ten','[1, 1]',0,0,0,50,10,0,1.00,0,0.50,0,0.00),(4,'digits','mixed',0,0,'none','all','ten','[1, 1]',0,0,0,50,10,0,1.00,0,0.50,0,0.00);
/*!40000 ALTER TABLE `dynamic_digits_apps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `flash_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flash_apps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `digits_capacity` json NOT NULL,
  `task_count` int NOT NULL DEFAULT '20',
  `watch_time` double(8,2) NOT NULL DEFAULT '2.00',
  `watch_time_progress` tinyint(1) NOT NULL DEFAULT '0',
  `min_watch_time` double(8,2) NOT NULL DEFAULT '0.50',
  `input_time` double(8,2) NOT NULL DEFAULT '10.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `flash_apps` WRITE;
/*!40000 ALTER TABLE `flash_apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `flash_apps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `free_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_apps` (
  `app_id` bigint unsigned NOT NULL,
  KEY `free_apps_app_id_foreign` (`app_id`),
  CONSTRAINT `free_apps_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `free_apps` WRITE;
/*!40000 ALTER TABLE `free_apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `free_apps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `group_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_schedules` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint unsigned NOT NULL,
  `mon` tinyint(1) NOT NULL DEFAULT '0',
  `tue` tinyint(1) NOT NULL DEFAULT '0',
  `wed` tinyint(1) NOT NULL DEFAULT '0',
  `thu` tinyint(1) NOT NULL DEFAULT '0',
  `fri` tinyint(1) NOT NULL DEFAULT '0',
  `sat` tinyint(1) NOT NULL DEFAULT '0',
  `sun` tinyint(1) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `start_at` time NOT NULL,
  `finish_at` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_schedules_group_id_foreign` (`group_id`),
  CONSTRAINT `group_schedules_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `group_schedules` WRITE;
/*!40000 ALTER TABLE `group_schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_schedules` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `groups_teacher_id_foreign` (`teacher_id`),
  CONSTRAINT `groups_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,1,NULL,'Нераспределенные ученики','2020-11-21 18:08:20','2020-11-21 18:08:20'),(2,2,NULL,'Нераспределенные ученики','2020-12-05 19:20:27','2020-12-05 19:20:27');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2015_06_16_131925_create_attachments_table',1),(2,'2020_09_27_075021_create_teachers_table',1),(3,'2020_09_27_075029_create_groups_table',1),(4,'2020_09_27_075035_create_students_table',1),(5,'2020_09_27_075045_create_apps_table',1),(6,'2020_09_27_075949_create_dynamic_digits_apps_table',1),(7,'2020_09_27_080026_create_app_sessions_table',1),(8,'2020_09_27_080109_create_subscription_transactions_table',1),(9,'2020_09_27_084244_create_student_app_table',1),(10,'2020_09_27_084245_create_group_schedules_table',1),(11,'2020_09_27_084246_create_student_visual_preferences_table',1),(12,'2020_10_11_075949_create_static_digits_apps_table',1),(13,'2020_10_11_075950_create_admins_table',1),(14,'2020_11_21_075817_create_brain_values_table',1),(15,'2020_03_07_201947_create_payments_table',2),(16,'2020_11_21_075818_create_subscriptions_table',2),(17,'2020_11_21_075819_add_subscriptions_left_to_teachers',2),(18,'2020_11_21_075819_delete_subscription_transaction_type',3),(20,'2020_11_21_075820_create_student_invites_table',4),(21,'2020_11_21_075821_make_login_nullable_in_students',5),(22,'2020_11_21_075822_create_free_apps_table',6),(23,'2020_12_14_201947_recreate_payments_table',6),(24,'2020_12_15_201947_create_flash_apps_table',6),(25,'2020_12_15_201947_create_multiplication_apps_table',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `multiplication_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multiplication_apps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `operation` enum('mul','div') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mul',
  `template1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_count` int NOT NULL DEFAULT '20',
  `watch_time` double(8,2) NOT NULL DEFAULT '5.00',
  `input_time` double(8,2) NOT NULL DEFAULT '10.00',
  `is_integer` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `multiplication_apps` WRITE;
/*!40000 ALTER TABLE `multiplication_apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `multiplication_apps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` bigint unsigned NOT NULL,
  `product_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` bigint unsigned DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` json DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refundable` tinyint(1) NOT NULL DEFAULT '0',
  `captured_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_owner_type_owner_id_index` (`owner_type`,`owner_id`),
  KEY `payments_product_type_product_id_index` (`product_type`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `static_digits_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_digits_apps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `mode` enum('digits','combinations','mixed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'digits',
  `operations` enum('sum','sub','mixed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mixed',
  `operation_alt` tinyint(1) NOT NULL DEFAULT '0',
  `bit_alt` tinyint(1) NOT NULL DEFAULT '0',
  `without_cross_in` enum('none','10','50','100','500','1000') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `bit_formulas` enum('all','except_up') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `formulas` enum('none','all','five','ten') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `horizontal` tinyint(1) NOT NULL DEFAULT '1',
  `digits_capacity` json NOT NULL,
  `fraction` int NOT NULL DEFAULT '0',
  `negative` tinyint(1) NOT NULL DEFAULT '0',
  `mirror` tinyint(1) NOT NULL DEFAULT '0',
  `task_count` int NOT NULL DEFAULT '20',
  `task_size` int NOT NULL DEFAULT '5',
  `task_size_progress` tinyint(1) NOT NULL DEFAULT '0',
  `watch_time` double(8,2) NOT NULL DEFAULT '2.00',
  `watch_time_progress` tinyint(1) NOT NULL DEFAULT '0',
  `min_watch_time` double(8,2) NOT NULL DEFAULT '1.00',
  `disable_mixed_formulas` tinyint(1) NOT NULL DEFAULT '0',
  `input_time` double(8,2) NOT NULL DEFAULT '10.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `static_digits_apps` WRITE;
/*!40000 ALTER TABLE `static_digits_apps` DISABLE KEYS */;
INSERT INTO `static_digits_apps` VALUES (1,'digits','mixed',0,0,'none','all','all',0,'[1, 1]',0,0,0,20,3,0,5.00,0,0.50,0,10.00),(2,'digits','mixed',1,0,'none','all','all',0,'[1, 1]',0,0,0,20,5,0,5.00,0,0.50,0,10.00),(3,'digits','mixed',1,0,'none','all','ten',0,'[1, 1]',0,0,0,20,5,0,5.00,0,0.50,0,10.00);
/*!40000 ALTER TABLE `static_digits_apps` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `student_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_app` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `app_id` bigint unsigned NOT NULL,
  `student_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_app_app_id_foreign` (`app_id`),
  KEY `student_app_student_id_foreign` (`student_id`),
  CONSTRAINT `student_app_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_app_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `student_app` WRITE;
/*!40000 ALTER TABLE `student_app` DISABLE KEYS */;
INSERT INTO `student_app` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `student_app` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `student_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_invites` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `student_id` bigint unsigned NOT NULL,
  `group_id` bigint unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_invites_student_id_foreign` (`student_id`),
  KEY `student_invites_group_id_foreign` (`group_id`),
  CONSTRAINT `student_invites_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_invites_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `student_invites` WRITE;
/*!40000 ALTER TABLE `student_invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_invites` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `student_visual_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_visual_preferences` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `student_id` bigint unsigned NOT NULL,
  `color_scheme` enum('standard','light','dark','brown') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'standard',
  `interface_scale` enum('standard','bigger','biggest') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'standard',
  `pictures` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `student_visual_preferences_student_id_foreign` (`student_id`),
  CONSTRAINT `student_visual_preferences_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `student_visual_preferences` WRITE;
/*!40000 ALTER TABLE `student_visual_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_visual_preferences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscription_until` timestamp NULL DEFAULT NULL,
  `group_id` bigint unsigned DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `students_group_id_foreign` (`group_id`),
  CONSTRAINT `students_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Ивано','Иван',NULL,'student','$2y$10$oChHY72FMA0obzazjeyn6uQ31GVCtT0ERECXPVOI4YI8OWcAPMdBq',NULL,NULL,NULL,'LAyhu0HehRRR4wD3x8Bt5jeYzl6KqwHh','Сургут',NULL,1,NULL,'2020-11-24 15:52:14','2020-11-24 15:52:14'),(2,'test','test',NULL,'test','$2y$10$Qo7zzrDUQYl/Rr2sreWfBuhafLdTwDX28l57hMy1kedvkqNuifQce',NULL,NULL,'','qDofLCv9LQdO8PzpY86S4qkA1oL7mu9J','teacher@1creator.ru',NULL,NULL,NULL,'2020-12-05 19:26:50','2020-12-06 15:16:35'),(3,'Ученик','Ученик',NULL,'','$2y$10$Un83FpxlQeqOvldLRxg9R.dyfJl7DQGt8JJ37M5k28wg2PdROJR5e',NULL,NULL,'volk_234@mail.ru','V1hC5HrsEEujm3CpvJ5tsh8IhQh7ohYc','Сургут',NULL,2,NULL,'2020-12-06 09:21:34','2020-12-06 15:21:24');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `subscription_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` bigint unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subscription_transactions_owner_type_owner_id_index` (`owner_type`,`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `subscription_transactions` WRITE;
/*!40000 ALTER TABLE `subscription_transactions` DISABLE KEYS */;
INSERT INTO `subscription_transactions` VALUES (1,'App\\Models\\Teacher',2,'Получен абонемент','2020-12-05 19:21:23'),(2,'App\\Models\\Teacher',2,'Передача абонемента ученику: ','2020-12-05 19:27:14'),(3,'App\\Models\\Student',2,'Приём абонемента от преподавателя: ','2020-12-05 19:27:14'),(4,'App\\Models\\Teacher',2,'Передача абонемента ученику: ','2020-12-05 19:33:18'),(5,'App\\Models\\Student',2,'Приём абонемента от преподавателя: ','2020-12-05 19:33:18'),(6,'App\\Models\\Teacher',2,'Получен абонемент','2020-12-05 19:36:53'),(7,'App\\Models\\Teacher',2,'Передача абонемента ученику: test t.','2020-12-05 19:37:15'),(8,'App\\Models\\Student',2,'Приём абонемента от преподавателя: test t.','2020-12-05 19:37:15'),(9,'App\\Models\\Student',1,'Получен абонемент','2020-12-06 14:52:13'),(10,'App\\Models\\Teacher',1,'Получен абонемент','2020-12-15 18:13:30');
/*!40000 ALTER TABLE `subscription_transactions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `expired_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_user_type_user_id_index` (`user_type`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES (5,'App\\Models\\Teacher',2,'2020-12-07 00:00:00','2020-12-05 19:36:53','2020-12-05 19:36:53'),(6,'App\\Models\\Student',2,'2021-01-05 00:00:00','2020-12-05 19:37:15','2020-12-05 19:37:15'),(7,'App\\Models\\Student',1,'2020-12-07 00:00:00','2020-12-06 14:52:13','2020-12-06 14:52:13'),(8,'App\\Models\\Teacher',1,'2020-12-31 00:00:00','2020-12-15 18:13:30','2020-12-15 18:13:30');
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subscriptions_left` int unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (1,'Волков','Александр',NULL,'$2y$10$JLsgXSPIMfxNqAvbY9KNPuYBrWRq7mIEvSFGBC2txNRYmokxFih9e',NULL,NULL,'volkov@1creator.ru',NULL,NULL,'ieLHDS4Rz93wQxbOBlsy9GRiwjO9HbvW',1,NULL,'2020-11-21 18:08:19','2020-11-21 18:08:19',0),(2,'test','test',NULL,'$2y$10$99T0kA5CPKlkGp9YDVyuDO0Sz.rwII8AbE6bIswEX/QYeHug1dcWq',NULL,NULL,'test@1creator.ru','test',NULL,'yIJd9WFRkZXRdnykVffXrvJkPU13Fpdn',1,NULL,'2020-12-05 19:20:27','2020-12-05 19:37:15',2);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

