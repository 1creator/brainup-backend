<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

require_once('student.php');
require_once('admin.php');
require_once('teacher.php');

$router->get('/payments/{payment}/account-type', 'PaymentController@getAccountType');
$router->post('/payment/notification', 'PaymentController@resolveNotification');

$router->get('/payment/success', 'PaymentController@success');
$router->get('/social-auth', 'SocialAuthController@redirect');
$router->get('/social-auth/callback/{provider}', 'SocialAuthController@login');
$router->post('/upload', 'UploadController@store');
$router->post('/feedback', 'FeedbackController@store');

$router->post('/login', 'AuthController@login');
$router->post('/register', 'AuthController@register');
$router->get('/reset-password', 'AuthController@resetPassword');
$router->post('/reset-password', 'AuthController@resetPasswordConfirm');
$router->get('/profile', 'ProfileController@index');
$router->patch('/profile', 'ProfileController@update');
