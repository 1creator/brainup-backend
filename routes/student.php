<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\Router;

$router->group(['namespace' => 'Student', 'prefix' => 'student'], function (Router $router) {
    $router->get('/variables', 'VariablesController@index');

    Route::group(['middleware' => 'auth:student'], function (Router $router) {
        $router->put('/profile', 'ProfileController@update');

        $router->get('/dashboard', 'DashboardController@index');

        $router->get('/leader-board', 'LeaderBoardController@index');

        $router->get('/teacher', 'TeacherController@index');
        $router->post('/teacher/join', 'TeacherController@join');

        $router->get('/apps', 'AppController@index');
        $router->get('/apps/{app}', 'AppController@show');
        $router->post('/apps', 'AppController@store');
        $router->put('/apps/{app}', 'AppController@update');
        $router->delete('/apps/{app}', 'AppController@destroy');

        $router->get('/group', 'GroupController@index');
        $router->post('/group/leave', 'GroupController@leave');

        $router->get('/brains', 'BrainsController@index');

        $router->get('/invites', 'InviteController@index');
        $router->delete('/invites/{invite}', 'InviteController@destroy');
        $router->post('/invites/{invite}/accept', 'InviteController@accept');

        $router->get('/students', 'StudentController@index');

        $router->get('/app-sessions', 'AppSessionController@index');
        $router->get('/apps/{app}/play', 'AppSessionController@play');
        $router->get('/app-sessions/{appSession}', 'AppSessionController@show');
        $router->get('/app-sessions/{appSession}/task', 'AppSessionController@getTask');
        $router->put('/app-sessions/{appSession}/restart', 'AppSessionController@restart');
        $router->put('/app-sessions/{appSession}/settings', 'AppSessionController@updateSettings');
        $router->post('/app-sessions/{appSession}/answer', 'AppSessionController@storeAnswer');

//        $router->get('/subscriptions', 'SubscriptionController@index');
        $router->get('/subscriptions/transactions', 'SubscriptionController@transactions');
        $router->get('/subscriptions/buy-subscription', 'SubscriptionController@buySubscription');

        $router->get('/attestations', 'AttestationController@index');
        $router->get('/attestations/{attestation}', 'AttestationController@show');
        $router->delete('/attestations/{attestation}', 'AttestationController@destroy');
        $router->post('/attestations', 'AttestationController@store');
        $router->get('/attestation-stages/{stage}', 'AttestationStageController@show');
        $router->post('/attestation-stages/{stage}/answer', 'AttestationStageController@answer');
    });
});

