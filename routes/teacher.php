<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\Router;

$router->group(['namespace' => 'Teacher', 'prefix' => 'teacher'], function (Router $router) {
    $router->get('/variables', 'VariablesController@index');

    $router->get('/apps/{app}/task-list', 'AppController@getTaskList');

    Route::group(['middleware' => 'auth:teacher'], function (Router $router) {
        $router->put('/profile', 'ProfileController@update');

        $router->get('/dashboard', 'DashboardController@index');

        $router->get('/students', 'StudentController@index');
        $router->get('/students/{student}/apps', 'StudentAppController@index');
        $router->get('/students/{student}/attestations', 'StudentAttestationController@index');
        $router->post('/students/{student}/attestation-level', 'StudentAttestationController@grantLevel');
        $router->get('/students/{student}/sessions', 'StudentSessionController@index');
        $router->get('/students/{student}/brains', 'StudentBrainsController@index');
        $router->get('/students/{student}', 'StudentController@show');
        $router->put('/students/{student}', 'StudentController@update');
        $router->delete('/students/{student}', 'StudentController@destroy');
        $router->post('/students', 'StudentController@store');
        $router->post('/students/invite', 'StudentController@invite');

        $router->get('/groups', 'GroupController@index');
        $router->get('/groups/{group}', 'GroupController@show');
        $router->post('/groups', 'GroupController@store');
        $router->put('/groups/{group}', 'GroupController@update');
        $router->delete('/groups/{group}', 'GroupController@destroy');

        $router->get('/apps', 'AppController@index');
        $router->get('/apps/{app}', 'AppController@show');
        $router->post('/apps', 'AppController@store');
        $router->put('/apps/{app}', 'AppController@update');
        $router->delete('/apps/{app}', 'AppController@destroy');

        $router->get('/apps/{app}/students', 'AppStudentController@index');

        $router->get('/app-sessions', 'AppSessionController@index');
        $router->get('/apps/{app}/play', 'AppSessionController@play');
        $router->get('/app-sessions/{appSession}', 'AppSessionController@show');
        $router->get('/app-sessions/{appSession}/task', 'AppSessionController@getTask');
        $router->put('/app-sessions/{appSession}/restart', 'AppSessionController@restart');
        $router->put('/app-sessions/{appSession}/settings', 'AppSessionController@updateSettings');
        $router->post('/app-sessions/{appSession}/answer', 'AppSessionController@storeAnswer');

//        $router->get('/subscriptions', 'SubscriptionController@index');
        $router->get('/subscriptions/buy-package', 'SubscriptionController@buyPackage');
        $router->get('/subscriptions/buy-subscription', 'SubscriptionController@buySubscription');
        $router->get('/subscriptions/transactions', 'SubscriptionController@transactions');
        $router->post('/subscriptions/give', 'SubscriptionController@give');

        $router->get('/attestations', 'AttestationController@index');
        $router->get('/attestations/{attestation}', 'AttestationController@show');
        $router->delete('/attestations/{attestation}', 'AttestationController@destroy');
        $router->post('/attestations', 'AttestationController@store');
        $router->post('/attestations/assign', 'AttestationController@assign');
        $router->post('/attestations/generate-variants', 'AttestationController@generateVariants');
        $router->get('/attestation-stages/{stage}', 'AttestationStageController@show');
        $router->post('/attestation-stages/{stage}/answer', 'AttestationStageController@answer');
    });
});
