<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\Router;

$router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function (Router $router) {
    $router->get('/variables', 'VariablesController@index');

    Route::group(['middleware' => 'auth:admin'], function (Router $router) {
        $router->put('/profile', 'ProfileController@update');

        $router->get('/dashboard', 'DashboardController@index');

        $router->patch('/variables', 'VariablesController@update');
        $router->get('/students', 'StudentController@index');
        $router->post('/students', 'StudentController@store');
        $router->get('/students/{student}', 'StudentController@show');
        $router->put('/students/{student}', 'StudentController@update');
        $router->delete('/students/{student}', 'StudentController@destroy');
        $router->get('/students/{student}/attestations', 'StudentAttestationController@index');
        $router->post('/students/{student}/subscriptions', 'StudentSubscriptionController@store');
        $router->delete('/students/{student}', 'StudentController@destroy');

        $router->get('/free-apps', 'FreeAppController@index');
        $router->post('/free-apps', 'FreeAppController@store');
        $router->get('/free-apps/{app}', 'FreeAppController@show');
        $router->put('/free-apps/{app}', 'FreeAppController@update');
        $router->delete('/free-apps/{app}', 'FreeAppController@destroy');

        $router->get('/payments', 'PaymentController@index');
        $router->get('/payments/{student}', 'PaymentController@show');
        $router->put('/payments/{student}', 'PaymentController@update');
//        $router->delete('/students/{student}', 'StudentController@destroy');

        $router->get('/apps/{app}/play', 'AppSessionController@play');
        $router->get('/app-sessions/{appSession}', 'AppSessionController@show');
        $router->get('/app-sessions/{appSession}/task', 'AppSessionController@getTask');
        $router->put('/app-sessions/{appSession}/restart', 'AppSessionController@restart');
        $router->put('/app-sessions/{appSession}/settings', 'AppSessionController@updateSettings');
        $router->post('/app-sessions/{appSession}/answer', 'AppSessionController@storeAnswer');

        $router->get('/teachers', 'TeacherController@index');
        $router->post('/teachers', 'TeacherController@store');
        $router->get('/teachers/{teacher}', 'TeacherController@show');
        $router->put('/teachers/{teacher}', 'TeacherController@update');
        $router->delete('/teachers/{teacher}', 'TeacherController@destroy');
        $router->post('/teachers/{teacher}/subscriptions', 'TeacherSubscriptionController@store');
        $router->delete('/teachers/{teacher}', 'TeacherController@destroy');

        $router->delete('/subscriptions/{subscription}', 'SubscriptionController@destroy');

        $router->delete('/attestation-levels/{id}', 'AttestationLevelController@destroy');

        $router->get('/attestations', 'AttestationController@index');
        $router->get('/attestations/{attestation}', 'AttestationController@show');
        $router->delete('/attestations/{attestation}', 'AttestationController@destroy');
        $router->get('/attestation-stages/{stage}', 'AttestationStageController@show');
    });
});
