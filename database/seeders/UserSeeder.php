<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'last_name' => 'Volkov',
                'first_name' => 'Aleksandr',
                'email' => 'admin@1creator.ru',
                'password' => Hash::make('qwerty2H'),
                'api_token' => Str::random(32),
            ],
        ];
        foreach ($admins as $adminData) {
            $admin = new Admin($adminData);
            $admin->save();
        }
    }
}
