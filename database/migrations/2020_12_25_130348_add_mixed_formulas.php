<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddMixedFormulas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `dynamic_digits_apps` MODIFY COLUMN `formulas` enum('none', 'all', 'five', 'ten', 'mixed') NOT NULL;");
        DB::statement("ALTER TABLE `static_digits_apps` MODIFY COLUMN `formulas` enum('none', 'all', 'five', 'ten', 'mixed') NOT NULL;");

        Schema::table('dynamic_digits_apps', function (Blueprint $table) {
            $table->dropColumn('disable_mixed_formulas');
        });
        Schema::table('static_digits_apps', function (Blueprint $table) {
            $table->dropColumn('disable_mixed_formulas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
