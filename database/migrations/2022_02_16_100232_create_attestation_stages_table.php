<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttestationStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attestation_stages', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['addition', 'multiplication', 'division']);
            $table->jsonb('tasks');
            $table->jsonb('answers');
            $table->jsonb('user_answers')->nullable();
            $table->foreignId('attestation_id')->references('id')->on('attestations')->cascadeOnDelete()->cascadeOnUpdate();
            $table->integer('score')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attestation_stages');
    }
}
