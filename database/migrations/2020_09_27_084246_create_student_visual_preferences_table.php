<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentVisualPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_visual_preferences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id')->references('id')
                ->on('students')->cascadeOnDelete()->cascadeOnUpdate();
            $table->enum('color_scheme', ['standard', 'light', 'dark', 'brown'])->default('standard');
            $table->enum('interface_scale', ['standard', 'bigger', 'biggest'])->default('standard');
            $table->boolean('pictures')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_visual_preferences');
    }
}
