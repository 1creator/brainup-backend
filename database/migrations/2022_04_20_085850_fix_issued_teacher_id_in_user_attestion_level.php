<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixIssuedTeacherIdInUserAttestionLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_attestation_level', function (Blueprint $table) {
            $table->dropForeign('user_attestation_level_issued_teacher_id_foreign');
            $table->foreign('issued_teacher_id')
                ->references('id')
                ->on('teachers')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_attestation_level', function (Blueprint $table) {
            //
        });
    }
}
