<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicDigitsAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_digits_apps', function (Blueprint $table) {
            $table->id();
            $table->enum('mode', ['digits', 'combinations', 'mixed'])->default('digits');
            $table->enum('operations', ['sum', 'sub', 'mixed'])->default('mixed');
            $table->boolean('operation_alt')->default(false);
            $table->boolean('bit_alt')->default(false);
            $table->enum('without_cross_in', ['none', '10', '50', '100', '500', '1000'])->default('none');
            $table->enum('bit_formulas', ['all', 'except_up'])->default('all');
            $table->enum('formulas', ['none', 'all', 'five', 'ten'])->default('all');
            $table->json('digits_capacity');
            $table->integer('fraction')->default(0);
            $table->boolean('negative')->default(false);
            $table->boolean('mirror')->default(false);
            $table->integer('task_count')->default(20);
            $table->integer('task_size')->default(5);
            $table->boolean('task_size_progress')->default(false);
            $table->float('watch_time')->default(2);
            $table->boolean('watch_time_progress')->default(false);
            $table->float('min_watch_time')->default(0.5);
            $table->boolean('disable_mixed_formulas')->default(false);
            $table->float('input_time')->default(10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_digits_apps');
    }
}
