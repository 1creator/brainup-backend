<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('owner_type');
            $table->unsignedBigInteger('owner_id');

            $table->index(['owner_type', 'owner_id']);

            $table->float('amount');
            $table->string('currency');
            $table->string('description')->nullable();
            $table->json('payment_method');
            $table->timestamps();
            $table->enum('status', ['pending', 'waiting_for_capture', 'succeeded', 'canceled']);
            $table->string('captured_at')->nullable();
            $table->json('confirmation')->nullable();
            $table->boolean('paid');
            $table->boolean('refundable');
            $table->boolean('receipt_registration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
