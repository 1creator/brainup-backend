<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAttestationLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_attestation_level', function (Blueprint $table) {
            $table->id();
            $table->morphs('user');
            $table->integer('level');
            $table->foreignId('issued_teacher')
                ->nullable()
                ->references('id')
                ->on('teachers')
                ->nullOnDelete()
                ->cascadeOnUpdate();
            $table->foreignId('attestation_id')
                ->nullable()
                ->references('id')
                ->on('attestations')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_attestation_level');
    }
}
