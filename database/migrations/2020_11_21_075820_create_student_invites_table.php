<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_invites', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id')->references('id')->on('students')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('group_id')->references('id')->on('groups')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_invites');
    }
}
