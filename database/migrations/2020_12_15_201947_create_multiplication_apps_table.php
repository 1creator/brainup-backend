<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultiplicationAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiplication_apps', function (Blueprint $table) {
            $table->id();
            $table->enum('operation', ['mul', 'div'])->default('mul');
            $table->string('template1');
            $table->string('template2');
            $table->integer('task_count')->default(20);
            $table->float('watch_time')->default(5);
            $table->float('input_time')->default(10);
            $table->boolean('is_integer')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiplication_apps');
    }
}
