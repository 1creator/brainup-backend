<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlashAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_apps', function (Blueprint $table) {
            $table->id();
            $table->json('digits_capacity');
            $table->integer('task_count')->default(20);
            $table->float('watch_time')->default(2);
            $table->boolean('watch_time_progress')->default(false);
            $table->float('min_watch_time')->default(0.5);
            $table->float('input_time')->default(10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_apps');
    }
}
