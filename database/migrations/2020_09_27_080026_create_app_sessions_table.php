<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_sessions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('app_id')->references('id')->on('apps')
                ->cascadeOnDelete()->cascadeOnUpdate();
            $table->morphs('player');

            $table->json('settings');

            $table->json('tasks');
            $table->json('answers');
            $table->json('state')->nullable();

            $table->float('quality')->default(0);
            $table->boolean('finished')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_sessions');
    }
}
