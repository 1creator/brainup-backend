<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticDigitsAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_digits_apps', function (Blueprint $table) {
            $table->id();
            $table->enum('mode', ['digits', 'combinations', 'mixed'])->default('digits'); // режим +
            $table->enum('operations', ['sum', 'sub', 'mixed'])->default('mixed'); // допустимые операции +
            $table->boolean('operation_alt')->default(false); //чередование операций +
            $table->boolean('bit_alt')->default(false); //чередование разрядности слагаемых +
            $table->enum('without_cross_in', ['none', '10', '50', '100', '500', '1000'])->default('none'); // не используется
            $table->enum('bit_formulas', ['all', 'except_up'])->default('all'); // формулы на всех разрядах или кроме старшего +
            $table->enum('formulas', ['none', 'all', 'five', 'ten'])->default('all'); //формулы в 5, в 10, все, без формул +
            $table->boolean('horizontal')->default(true); // отображение примера
            $table->json('digits_capacity'); // разрядность слагаемых от-до +
            $table->integer('fraction')->default(0); // кол-вол знаков после запятой при включеной галочке "Дробные числа" +
            $table->boolean('negative')->default(false);  // отриц числа в результате +
            $table->boolean('mirror')->default(false); // зеркальные числа +
            $table->integer('task_count')->default(20); //кол-во примеров +
            $table->integer('task_size')->default(5); // слагаемых в примере +
            $table->boolean('task_size_progress')->default(false); // Прогресс кол-ва слагаемых +
            $table->float('watch_time')->default(2); // время на просмотр слагаемого +
            $table->boolean('watch_time_progress')->default(false); // Прогресс скорости +
            $table->float('min_watch_time')->default(1); //минимальное время при включенной галочке "Прогресс скорости" +
            $table->boolean('disable_mixed_formulas')->default(false); // Не использовать смешанные формулы +
            $table->float('input_time')->default(10); // время на ввод ответа +
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_digits_apps');
    }
}
